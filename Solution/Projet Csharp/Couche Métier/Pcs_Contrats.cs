﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Projet_Csharp.Couche_Métier
{
    /// <summary>
    /// Gere toutes les actions liées aux contrats
    /// </summary>
    class Pcs_Contrats
    {
        private Projet_Csharp.CAD.Mpg_Contrats oContrat;
        private Projet_Csharp.CAD.CL_CAD oCad;
        /// <summary>
        /// Constructeur par défaut, initialise tous les objets
        /// </summary>
        public Pcs_Contrats()
        {
            this.oContrat = new Projet_Csharp.CAD.Mpg_Contrats();
            this.oCad = new Projet_Csharp.CAD.CL_CAD();
        }
        /// <summary>
        /// Permet d'obtenir les contrats et leur ID
        /// </summary>
        /// <param name="rowsname"></param>
        /// <returns></returns>
        public DataSet m_GetContrats(string rowsname)
        {
            return oCad.m_GetRows(oContrat.m_GetContrats(), rowsname); ;
        }
    }
}
