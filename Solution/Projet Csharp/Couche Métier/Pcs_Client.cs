﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Projet_Csharp.Couche_Métier
{
    /// <summary>
    /// Class regroupant l'ensemble des actions liées aux clients
    /// </summary>
    class Client
    {
        private Projet_Csharp.CAD.Mpg_Client oMpgRageux;
        private Projet_Csharp.CAD.CL_CAD oCad;

        /// <summary>
        /// constructeur par défaut, initialise les différends objet
        /// </summary>
        public Client()
        {
            this.oMpgRageux = new Projet_Csharp.CAD.Mpg_Client();
            this.oCad = new Projet_Csharp.CAD.CL_CAD();
        }
        /// <summary>
        /// Affiche les infos d'un client
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="prenom"></param>
        /// <param name="rowsNom"></param>
        /// <returns></returns>
        public System.Data.DataSet action_AfficherClient(string nom,string prenom, string rowsNom)
        {
            return this.oCad.m_GetRows(this.oMpgRageux.m_GetCustomer(nom,prenom), rowsNom);
        }
        /// <summary>
        /// Permet l'ajout d'un client
        /// </summary>
        /// <param name="prenom"></param>
        /// <param name="nom"></param>
        /// <param name="adresse"></param>
        /// <param name="tel"></param>
        /// <param name="datenaissance"></param>
        /// <param name="permiscotier"></param>
        public void action_AjouterClient(string prenom, string nom, string adresse, string tel, string datenaissance, string permiscotier)
        {
            this.oCad.m_ActionRows(this.oMpgRageux.m_AjouterClient(prenom, nom, adresse, tel, datenaissance, permiscotier));
        }
        /// <summary>
        /// Permet d'afficher les noms de tous les clients
        /// </summary>
        /// <param name="rowsNom"></param>
        /// <returns></returns>
        public System.Data.DataSet action_AfficherTout(string rowsNom)
        {
            return this.oCad.m_GetRows(this.oMpgRageux.m_GetAllCustomersName(), rowsNom);
        }
        /// <summary>
        /// permet de modifier un client
        /// </summary>
        /// <param name="prenom"></param>
        /// <param name="nom"></param>
        /// <param name="adresse"></param>
        /// <param name="tel"></param>
        /// <param name="datenaissance"></param>
        /// <param name="permiscotier"></param>
        public void action_ModifierClient(string prenom, string nom, string adresse, string tel, string datenaissance, string permiscotier)
        {
            this.oCad.m_ActionRows(this.oMpgRageux.m_MofifierClient(prenom, nom, adresse, tel, datenaissance, permiscotier));
        }
        /// <summary>
        /// Supprime un client
        /// </summary>
        /// <param name="nom"></param>
        public void action_SupprimerClient(string nom)
        {
            this.oCad.m_ActionRows(this.oMpgRageux.m_SupprimerClient(nom));
        }
        /// <summary>
        /// Affiche les prénoms correspondant a un nom
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="rowsname"></param>
        /// <returns></returns>
        public DataSet action_AfficherPrenom(string nom, string rowsname)
        {
            return this.oCad.m_GetRows(this.oMpgRageux.m_GetCustomersByName(nom), rowsname);
        }
    }
}