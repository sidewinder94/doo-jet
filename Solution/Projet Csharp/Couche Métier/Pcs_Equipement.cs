﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Projet_Csharp.Couche_Métier
{
    /// <summary>
    /// Fonctions travaillant sur les equipements
    /// </summary>
    class Pcs_Equipement
    {
        private CAD.CL_CAD oCAD;
        private CAD.Mpg_Equipement oEquipement;
        private CAD.Mpg_Reservation_Equipement oReservationEquipement;
        private CAD.Mpg_Reservation_Effectif oReservationEffectif;
        private CAD.Mpg_reservation oReservation;
        private CAD.Mpg_Activité oActivité;
        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public Pcs_Equipement()
        {
            oCAD = new CAD.CL_CAD();
            oEquipement = new CAD.Mpg_Equipement();
            oReservationEffectif = new CAD.Mpg_Reservation_Effectif();
            oReservationEquipement = new CAD.Mpg_Reservation_Equipement();
            oReservation = new CAD.Mpg_reservation();
            oActivité = new CAD.Mpg_Activité();
        }
        /// <summary>
        /// Afficher tous les equipements
        /// </summary>
        /// <param name="rowsname"></param>
        /// <returns></returns>
        public System.Data.DataSet m_GetAllEquipments(string rowsname)
        {
            return oCAD.m_GetRows(oEquipement.m_getAllEquimpent(), rowsname);
        }
        /// <summary>
        /// Récupère le nombre d'équipements disponibles en se basant sur le nom
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public int m_GetDisponibilityByName(string name)
        {
            return oCAD.m_Scalar_int(oEquipement.m_GetDisponibilityByName(name));
        }
        /// <summary>
        /// récupère le prix unitaire d'un equipement en se basant sur son nom
        /// </summary>
        /// <param name="name"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        public string m_GetPrice(string name, string quantity)
        {
            return Convert.ToString(oCAD.m_Scalar_double(oEquipement.m_GetPrice(name))
                *Convert.ToDouble(quantity));
        }
        /// <summary>
        /// Ajouter un equipement à une commande en fonction de son nom
        /// </summary>
        /// <param name="commande_ID"></param>
        /// <param name="equipmentName"></param>
        /// <param name="number"></param>
        public bool m_AddEquimpentByName(int commande_ID, string equipmentName, int number,double cost)
        {
            int quantity = oCAD.m_Scalar_int(oEquipement.m_GetDisponibilityByName(equipmentName));
            if (quantity >= number)
            {
                int equipmentID = oCAD.m_Scalar_int(oEquipement.m_GetIDByName(equipmentName));

                oCAD.m_ActionRows(oReservationEquipement.m_AddByName(commande_ID,
                    equipmentID, number,cost));

                quantity -= number;

                oCAD.m_ActionRows(oEquipement.m_SetDisponibility(equipmentID,quantity));
                double temp = oCAD.m_Scalar_double(oReservation.m_GetPriceByID(commande_ID));
                oCAD.m_ActionRows(oReservation.m_SetCostByID(temp + cost ,commande_ID));
                return true;
            }
            else
            {
                return false;
            }
        }
        /// <summary>
        /// Ajout d'un equipement
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="descriptif"></param>
        /// <param name="puissance"></param>
        /// <param name="disponibilité"></param>
        /// <param name="CoutHT"></param>
        /// <param name="TypeAct"></param>
        /// <param name="nbtotal"></param>
        public void m_ajouterEquipement(string nom, string descriptif, string puissance, int disponibilité, int CoutHT, int TypeAct, int nbtotal)
        {
            this.oCAD.m_ActionRows(this.oEquipement.m_InsertEquipement(nom, descriptif, puissance, disponibilité, CoutHT, TypeAct,nbtotal));
        }
        /// <summary>
        /// Supprime un equipement en fonction de son nom
        /// </summary>
        /// <param name="nom"></param>
        public void m_SupprimerEquipement(string nom)
        {
            this.oCAD.m_ActionRows(this.oEquipement.m_DeleteEquipement(nom));
        }
        /// <summary>
        /// Met un jour d'un equipement en se basant sur son nom
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="descriptif"></param>
        /// <param name="puissance"></param>
        /// <param name="disponibilité"></param>
        /// <param name="CoutHT"></param>
        /// <param name="TypeAct"></param>
        /// <param name="nbtotal"></param>
        public void m_MettreAjourEquipement(string nom, string descriptif, string puissance, int disponibilité, int CoutHT, int TypeAct, int nbtotal)
        {
            this.oCAD.m_ActionRows(this.oEquipement.m_UpdateEquipement(nom, descriptif, puissance, disponibilité, CoutHT, TypeAct,nbtotal));
        }
            /// <summary>
            /// récupère les infos concernant un equipement
            /// </summary>
            /// <param name="name"></param>
            /// <param name="rowsname"></param>
            /// <returns></returns>
        public System.Data.DataSet m_GetOneEquipement(string name, string rowsname)
        {   
            return this.oCAD.m_GetRows(this.oEquipement.m_GetOneEquipement(name), rowsname);
        }
        /// <summary>
        /// Récupère toutes les données concernant tous les équipements
        /// </summary>
        /// <param name="rowsname"></param>
        /// <returns></returns>
        public DataSet m_GetAllEquipmentsData(string rowsname)
        {
            return oCAD.m_GetRows(oEquipement.m_getAllEquimpentData(), rowsname);
        }
        /// <summary>
        /// Récupère l'ensemble des activités
        /// </summary>
        /// <param name="rowsname"></param>
        /// <returns></returns>
        public DataSet m_GetActivités(string rowsname)
        {
            return oCAD.m_GetRows(oActivité.m_getAll(),rowsname);
        }
    } 
}
