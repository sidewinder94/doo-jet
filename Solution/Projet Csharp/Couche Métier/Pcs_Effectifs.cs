﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Projet_Csharp.Couche_Métier
{
    /// <summary>
    /// Contient toutes les méthodes effectifs
    /// </summary>
    class Pcs_Effectifs
    {
        private CAD.CL_CAD oCAD;
        private CAD.Mpg_Effectif oEffectif;
        private CAD.Mpg_Reservation_Effectif oReservation_Effectif;

        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public Pcs_Effectifs()
        {
            oCAD = new CAD.CL_CAD();
            oEffectif = new CAD.Mpg_Effectif();
            oReservation_Effectif = new CAD.Mpg_Reservation_Effectif();
        }
        /// <summary>
        /// Fonction actuellement de stub, est censée retourner les nom disponibles pour une plage
        /// horaire, retourne actuellement tous les nom
        /// </summary>
        /// <param name="begin_date"></param>
        /// <param name="begin_hour"></param>
        /// <param name="end_date"></param>
        /// <param name="end_hour"></param>
        /// <param name="rowsname"></param>
        /// <returns></returns>
        public System.Data.DataSet m_GetNameByDisponibility(string begin_date, 
            string begin_hour, string end_date,string end_hour, string rowsname)
        {
            return oCAD.m_GetRows(oEffectif.m_GetNameByDisponibility(begin_date,
                begin_hour,end_date,end_hour),rowsname);
        }

        /// <summary>
        /// Fonction actuellement de stub, est censée retourner les prénoms disponibles pour une plage
        /// horaire, retourne actuellement tous les prénom correspondants aux nom
        /// </summary>
        /// <param name="begin_date"></param>
        /// <param name="begin_hour"></param>
        /// <param name="end_date"></param>
        /// <param name="end_hour"></param>
        /// <param name="name"></param>
        /// <param name="rowsname"></param>
        /// <returns></returns>
        public System.Data.DataSet m_GetFirstNameByDisponibilityAndName(string begin_date,
            string begin_hour, string end_date, string end_hour,string name, string rowsname)
        {
            return oCAD.m_GetRows(oEffectif.m_GetFirstNameByDisponibility(begin_date,
                begin_hour, end_date, end_hour,name), rowsname);
        }

        /// <summary>
        /// Ajouter un effectif
        /// </summary>
        /// <param name="Secu"></param>
        /// <param name="Embauche"></param>
        /// <param name="VisiteMedical"></param>
        /// <param name="contratTravail"></param>
        /// <param name="PermisCotier"></param>
        /// <param name="BEES"></param>
        /// <param name="StatutActivite"></param>
        /// <param name="Prenom"></param>
        /// <param name="Nom"></param>
        public void m_ajouterEffectif(string Secu, string Embauche, string VisiteMedical, int contratTravail, string PermisCotier, string BEES, string StatutActivite, string Prenom, string Nom)
        {
            this.oCAD.m_ActionRows(this.oEffectif.AjouterEffectif(Secu, Embauche, VisiteMedical, contratTravail, PermisCotier, BEES, StatutActivite, Prenom, Nom));
        }
        /// <summary>
        /// Afficher tous les effectifs
        /// </summary>
        /// <param name="rowsNom"></param>
        /// <returns></returns>
        public System.Data.DataSet m_afficherEffectifs(string rowsNom)
        {
            return this.oCAD.m_GetRows(this.oEffectif.AfficherEffectif(), rowsNom);
        }
        /// <summary>
        /// Afficher un effectif
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="rowsNom"></param>
        /// <returns></returns>
        public System.Data.DataSet m_afficherUnEffectif(string nom,string prenom, string rowsNom)
        {
            return this.oCAD.m_GetRows(this.oEffectif.AfficherParNomPrenom(nom,prenom), rowsNom);
        }
        /// <summary>
        /// Supprimer un effectif
        /// </summary>
        /// <param name="nom"></param>
        public void m_supprimerEffectif(string nom)
        {
            this.oCAD.m_ActionRows(this.oEffectif.EffacerEffectif(nom));
        }
        /// <summary>
        /// Mettre à jour un effectif
        /// </summary>
        /// <param name="Secu"></param>
        /// <param name="Embauche"></param>
        /// <param name="VisiteMedical"></param>
        /// <param name="contratTravail"></param>
        /// <param name="PermisCotier"></param>
        /// <param name="BEES"></param>
        /// <param name="StatutActivite"></param>
        /// <param name="Prenom"></param>
        /// <param name="Nom"></param>
        public void m_MettreAjourEffectif(string Secu, string Embauche, string VisiteMedical, int contratTravail, string PermisCotier, string BEES, string StatutActivite, string Prenom, string Nom,int ID)
        {
            this.oCAD.m_ActionRows(this.oEffectif.MettreAjour( Secu, Embauche, VisiteMedical, contratTravail, PermisCotier, BEES, StatutActivite, Prenom, Nom,ID));
        }
        
        /// <summary>
        /// Ajoute une association entre un effectif et une réservation
        /// </summary>
        /// <param name="lastname"></param>
        /// <param name="firstname"></param>
        /// <param name="commmandID"></param>
        public void m_AddToReservation(string lastname, string firstname, string commmandID)
        {
            int salaryID = oCAD.m_Scalar_int(oEffectif.m_GetSalaryID(firstname,lastname));
            oCAD.m_ActionRows(oReservation_Effectif.AddAssociation(salaryID, commmandID));
        }
        /// <summary>
        /// Récupère tous les noms
        /// </summary>
        /// <param name="rowsname"></param>
        /// <returns></returns>
        public DataSet m_GetAllNames(string rowsname)
        {
            return oCAD.m_GetRows(oEffectif.AfficherEffectif(), rowsname);
        }
        /// <summary>
        /// Récupère tous les prénoms correspondant au nom passé en paramètre
        /// </summary>
        /// <param name="lastname"></param>
        /// <param name="rowsname"></param>
        /// <returns></returns>
        public DataSet m_GetFirstNameByName(string lastname, string rowsname)
        {
            return oCAD.m_GetRows(oEffectif.AfficherParNomPrenom(lastname),rowsname) ;
        }
        /// <summary>
        /// Récupère l'ID associé a un couple nom/prénom
        /// </summary>
        /// <param name="Lastname"></param>
        /// <param name="FirstName"></param>
        /// <returns></returns>
        public int m_GetID(string Lastname, string FirstName)
        {
            return oCAD.m_Scalar_int(oEffectif.m_GetSalaryID(FirstName,Lastname));
        }
    }
}
