﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Projet_Csharp.Couche_Métier
{
    /// <summary>
    /// Classe contenant les méthodes utilisées par l'impression
    /// </summary>
    class Pcs_Impression
    {
        /// <summary>
        /// Méthode permettant de créer un fichier temporaire et 
        /// de le remplir avec le contenu du paramètre
        /// Retourne l'emplacement du fichier temporaire
        /// </summary>
        /// <param name="textToWrite"></param>
        /// <returns></returns>
        public string GetTempFile(string textToWrite)
        {
            string temp = null;
            temp = System.IO.Path.GetTempFileName();
            System.IO.StreamWriter streamToWrite = new System.IO.StreamWriter(temp);
            streamToWrite.Write(textToWrite);
            streamToWrite.Close();
            return temp;
        }
    }
}
