﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.OleDb;


//TODO : Si matériel pas disponible sur le créneau horaire alors ne pas l'afficher dans la liste déroulante
namespace Projet_Csharp.Couche_Métier
{
    /// <summary>
    /// Class gérant les réservations
    /// </summary>
    class Pcs_Reservations
    {
        /*Définition des nom d'objets dont nous nous servirons*/
        private CAD.CL_CAD oCAD;
        private CAD.Mpg_reservation oReservation;
        private CAD.Mpg_Reservation_Effectif oReservationEffectif;
        private CAD.Mpg_Reservation_Equipement oReservationEquipement;
        private CAD.Mpg_Equipement oEquipement;
        private CAD.Mpg_Effectif oEffectif;
        private CAD.Mpg_Client oClient;
        /// <summary>
        /// Initialise les objets utilisés
        /// </summary>
        public Pcs_Reservations()
        {
            /*Initialisation des objets*/
            oCAD = new CAD.CL_CAD();
            oReservation = new CAD.Mpg_reservation();
            oReservationEffectif = new CAD.Mpg_Reservation_Effectif();
            oReservationEquipement = new CAD.Mpg_Reservation_Equipement();
            oEquipement = new CAD.Mpg_Equipement();
            oEffectif = new CAD.Mpg_Effectif();
            oClient = new CAD.Mpg_Client();
        }
        /// <summary>
        /// Récupère mes nom de tous les clients
        /// </summary>
        /// <param name="rowsName"></param>
        /// <returns></returns>
        public DataSet m_GetAllCustomersName(string rowsName)
        {
            return oCAD.m_GetRows(oClient.m_GetAllCustomersName(), rowsName);
        }

        /// <summary>
        /// Récupère les infos d'un client en se basant sur un nom
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="rowsname"></param>
        /// <returns></returns>
        public DataSet m_GetCustomerByName(string nom, string rowsname)
        {
            return oCAD.m_GetRows(oClient.m_GetCustomersByName(nom), rowsname);
        }
        /// <summary>
        /// Ajoute une réservation
        /// </summary>
        /// <param name="BeginDay"></param>
        /// <param name="BeginHour"></param>
        /// <param name="EndDay"></param>
        /// <param name="EndHour"></param>
        /// <param name="Customer_FirstName"></param>
        /// <param name="Customer_LastName"></param>
        public void m_AddReservation(string BeginDay, string BeginHour, string EndDay, string EndHour, string Customer_FirstName, string Customer_LastName)
        {
            int customerID = oCAD.m_Scalar_int(oClient.m_GetCustomerID(Customer_FirstName, Customer_LastName));
            oCAD.m_ActionRows(oReservation.m_AddReservation(BeginDay, BeginHour, EndDay, EndHour, 0, customerID));
        }
        /// <summary>
        /// Récupère l'ID de la dernière réservation
        /// </summary>
        /// <returns></returns>
        public int m_GetLast()
        {
            return oCAD.m_Scalar_int(oReservation.m_GetLast());
        }
        /// <summary>
        /// Récupère les informations de toutes les réservations
        /// </summary>
        /// <param name="rowsname"></param>
        /// <returns></returns>
        public DataSet m_GetAll(string rowsname)
        {
            return oCAD.m_GetRows(oReservation.m_GetAll(), rowsname);
        }
        /// <summary>
        /// Récupère le cout d'une réservation par prix
        /// </summary>
        /// <param name="commandID"></param>
        /// <returns></returns>
        public double m_GetPriceByID(int commandID)
        {
            return oCAD.m_Scalar_double(oReservation.m_GetPriceByID(commandID));
        }

        /// <summary>
        /// Méthode permettant de créer un rapport concernant une réservation
        /// </summary>
        /// <param name="commandID"></param>
        /// <returns></returns>
        public string m_MakeReport(string commandID)
        {
            /*On obtient toutes les propriétés nécessaires pour identifier le client*/
            int customerID = oCAD.m_Scalar_int(oReservation.GetCustomerID(commandID));
            string customerlastname = oCAD.m_Scalar_string(oClient.m_GetCustomerLNByID(customerID));
            string customerfirstname = oCAD.m_Scalar_string(oClient.m_GetCustomerFNByID(customerID));
            /*On commence a entrer un texte générique personnalisé en fonction du client*/
            string command_text = "Commande N° " + commandID
                + "\nLe client n° " + customerID + " : " + customerfirstname + " "
                + customerlastname
                + "\n\nMatériel : \nType | Nombre | Cout\n\n";

            /*on compte le nombre d'équipements loués*/
            int rowcount = oCAD.m_Scalar_int(oReservation.m_CountByID(commandID));
            /*Si des équipements ont été loués*/
            if (rowcount != 0)
            {
                /*Pour chaque équipement loué on affche son nom, la quantité et le prix unitaire*/
                for (int i = 0; i < rowcount; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        command_text += Convert.ToString(oCAD.m_GetRows(oReservationEquipement.
                            m_GetReservationEquipment(commandID), "rows").Tables["rows"].Rows[i][j]);
                        if (j == 2)
                        {
                            command_text += " €\n";
                        }
                        else
                        {
                            command_text += " | ";
                        }
                    }
                }
            }
            else
            {
                command_text += "Aucun matériel loué pour cette réservation\n";
            }

            /*TODO : Liste des moniteurs réservés*/

            command_text += "\nMoniteurs participant à la réservation :\n"
                + "Nom | Prénom | Activité\n\n";
            /*on compte le nombre de moniteurs "réservés"*/
            rowcount = oCAD.m_Scalar_int(oReservationEffectif.m_CountByID(commandID));
            /*Si des moniteurs ont été réservés*/
            if (rowcount != 0)
            {
                /*Pour chaque moniteur réservé, on affiche : Nom, Prénom, Statut Activité*/
                for (int i = 0; i < rowcount; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        command_text += Convert.ToString(oCAD.m_GetRows(oReservationEffectif.
                            m_GetReservationEffectif(commandID), "rows").Tables["rows"].Rows[i][j]);
                        if (j == 2)
                        {
                            command_text += "\n";
                        }
                        else
                        {
                            command_text += " | ";
                        }
                    }
                }
            }
            else
            {
                command_text += "Aucun moniteur participant à l'activité";
            }
            command_text += "\n\n";
            command_text += "Total de la commande : " + oCAD.m_Scalar_double(oReservation.m_GetPriceByID(Convert.ToInt32(commandID))) + " €";
            return command_text;
        }
        /// <summary>
        /// Récupère les equipements liés à une réservation par rapport la l'ID
        /// de la  commande
        /// </summary>
        /// <param name="commandID"></param>
        /// <param name="rowsname"></param>
        /// <returns></returns>
        public DataSet m_GetEquipmentReservations(int commandID, string rowsname)
        {
            return oCAD.m_GetRows(oReservationEquipement.mGetAllProductByCommandID(commandID), rowsname);
        }
        /// <summary>
        /// Supprime un equipement d'une réservation
        /// </summary>
        /// <param name="commandID"></param>
        /// <param name="equipmentID"></param>
        public void m_deleteEquipmentReservation(int commandID, string equipmentID)
        {
            oCAD.m_ActionRows(oReservationEquipement.m_DeleteAssociation(commandID, equipmentID));
        }
        /// <summary>
        /// Récupère toutes les infos de toutes les réservations
        /// </summary>
        /// <param name="rowsname"></param>
        /// <returns></returns>
        public DataSet m_GetAllData(string rowsname)
        {
            return oCAD.m_GetRows(oReservation.m_GetAllData(), rowsname);
        }
    }
}
