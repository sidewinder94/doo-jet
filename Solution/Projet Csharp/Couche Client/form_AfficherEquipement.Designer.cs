﻿namespace Projet_Csharp.Couche_Client
{
    partial class form_AfficherEquipement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.button1_close = new System.Windows.Forms.Button();
            this.textBox1_NomEquip = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nom équipement";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 44);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(367, 207);
            this.dataGridView1.TabIndex = 3;
            // 
            // button1_close
            // 
            this.button1_close.Location = new System.Drawing.Point(329, 14);
            this.button1_close.Name = "button1_close";
            this.button1_close.Size = new System.Drawing.Size(50, 23);
            this.button1_close.TabIndex = 4;
            this.button1_close.Text = "Retour";
            this.button1_close.UseVisualStyleBackColor = true;
            this.button1_close.Click += new System.EventHandler(this.button1_close_Click);
            // 
            // textBox1_NomEquip
            // 
            this.textBox1_NomEquip.FormattingEnabled = true;
            this.textBox1_NomEquip.Location = new System.Drawing.Point(105, 16);
            this.textBox1_NomEquip.Name = "textBox1_NomEquip";
            this.textBox1_NomEquip.Size = new System.Drawing.Size(100, 21);
            this.textBox1_NomEquip.TabIndex = 5;
            this.textBox1_NomEquip.SelectedValueChanged += new System.EventHandler(this.textBox1_NomEquip_SelectedValueChanged);
            // 
            // form_AfficherEquipement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(390, 263);
            this.Controls.Add(this.textBox1_NomEquip);
            this.Controls.Add(this.button1_close);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.label1);
            this.Name = "form_AfficherEquipement";
            this.Text = "Afficher fiche détaillée d\'un Equipement";
            this.Load += new System.EventHandler(this.form_AfficherEquipement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1_close;
        private System.Windows.Forms.ComboBox textBox1_NomEquip;
    }
}