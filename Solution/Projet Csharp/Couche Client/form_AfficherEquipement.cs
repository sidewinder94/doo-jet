﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Projet_Csharp.Couche_Client
{
    /// <summary>
    /// Class D'affichage d'un equipement en particulier
    /// </summary>
    public partial class form_AfficherEquipement : Form
    {
        /// <summary>
        /// création des objets DATA et equipement
        /// </summary>
        private System.Data.DataSet oDATASET01;
        private Projet_Csharp.Couche_Métier.Pcs_Equipement oEquipement;

        /// <summary>
        /// Constructeur par défaut
        /// </summary>
        public form_AfficherEquipement()
        {
            this.oDATASET01 = new DataSet();
            this.oEquipement = new Projet_Csharp.Couche_Métier.Pcs_Equipement();
            InitializeComponent();
        }

        /// <summary>
        /// Constructeur paramétré permettant d'afficher un equipement en particulier
        /// </summary>
        /// <param name="equipmentname"></param>
        public form_AfficherEquipement(string equipmentname)
        {
            this.oDATASET01 = new DataSet();
            this.oEquipement = new Projet_Csharp.Couche_Métier.Pcs_Equipement();
            InitializeComponent();
            textBox1_NomEquip.Enabled = false;
            textBox1_NomEquip.Text = equipmentname;
            dataGridView1.DataSource = oEquipement.m_GetOneEquipement(equipmentname, 
                "rwsEquipement").Tables["rwsEquipement"];
        }
        /// <summary>
        /// Charge la combobox des noms des équipements au chargement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void form_AfficherEquipement_Load(object sender, EventArgs e)
        {
            textBox1_NomEquip.DataSource = oEquipement.m_GetAllEquipments("rows").Tables["rows"];
            textBox1_NomEquip.DisplayMember = "Nom";
        }

        /// <summary>
        /// appel de la fonction et de la requete pour la recherche
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_close_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// fermeture du formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_NomEquip_SelectedValueChanged(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = this.oEquipement.m_GetOneEquipement(this.textBox1_NomEquip.Text, "rwsEquipement").Tables["rwsEquipement"];
        }
    }
}
