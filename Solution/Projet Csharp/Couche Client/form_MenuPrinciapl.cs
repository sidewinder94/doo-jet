﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Projet_Csharp.Couche_Client
{
    /// <summary>
    /// Formulaire de menu principal
    /// </summary>
    public partial class Form1 : Form
    {
       /// <summary>
       /// Constructeur par défaut, initialise les composants
       /// </summary>
         public Form1()
        {
            InitializeComponent();
        }
       
        /// <summary>
        /// fermeture du formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_close_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// appel d'un formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_addeff_Click(object sender, EventArgs e)
        {
            form_AjouterEffectif form1 = new form_AjouterEffectif();
            form1.Show();
        }

        /// <summary>
        /// appel d'un formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_afficher_Click(object sender, EventArgs e)
        {
            form_AfficherToutEffectif form2 = new form_AfficherToutEffectif();
            form2.Show();
        }

        /// <summary>
        /// appel d'un formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            form_AfficherUnEffectif_Modif form2 = new form_AfficherUnEffectif_Modif();
            form2.Show();
        }

        /// <summary>
        /// appel d'un formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            Form_AjouterClient form1 = new Form_AjouterClient();
            form1.Show();
        }

        /// <summary>
        /// appel d'un formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button6_Click(object sender, EventArgs e)
        {
            Form_AfficherToutClient form1 = new Form_AfficherToutClient();
            form1.Show();
        }

        /// <summary>
        /// appel d'un formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button5_Click(object sender, EventArgs e)
        {
            Form_AfficherClient form1 = new Form_AfficherClient();
            form1.Show();
        }

        /// <summary>
        /// appel d'un formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click_2(object sender, EventArgs e)
        {
            form_AjoutSupprModifEquipement form1 = new form_AjoutSupprModifEquipement();
            form1.Show();
        }

        /// <summary>
        /// appel d'un formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button7_Click(object sender, EventArgs e)
        {
            form_AfficherToutEquipement form1 = new form_AfficherToutEquipement();
            form1.Show();
        }

        /// <summary>
        /// appel d'un formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click_1(object sender, EventArgs e)
        {
            form_AfficherEquipement form1 = new form_AfficherEquipement();
            form1.Show();
        }
        /// <summary>
        /// apple d'un formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button8_affichertoutresa_Click(object sender, EventArgs e)
        {
            form_AfficherReservations form1 = new form_AfficherReservations();
            form1.Show();
        }
        /// <summary>
        /// appel d'un formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Reservations_Button_Click(object sender, EventArgs e)
        {
            form_Reservation form1 = new form_Reservation();
            form1.Show();
        }

       
    }
}
