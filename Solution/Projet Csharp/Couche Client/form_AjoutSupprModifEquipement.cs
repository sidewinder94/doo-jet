﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Projet_Csharp.Couche_Client
{
    /// <summary>
    /// formulaire de gestion des équipements
    /// </summary>
    public partial class form_AjoutSupprModifEquipement : Form
    {
        private Projet_Csharp.Couche_Métier.Pcs_Equipement oEquipement;


        /// <summary>
        /// création de l'objet
        /// </summary>
        public form_AjoutSupprModifEquipement()
        {
            this.oEquipement = new Projet_Csharp.Couche_Métier.Pcs_Equipement();
            InitializeComponent();
        }
        /// <summary>
        /// Exécute la requête d'ajout de matériel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_add_Click(object sender, EventArgs e)
        {
            this.oEquipement.m_ajouterEquipement(this.textBox1_Nom.Text, this.textBox1_descript.Text, this.textBox1_Puissance.Text, Convert.ToInt32(this.textBox1_dispo.Text), Convert.ToInt32(this.textBox1_cout.Text), (int)this.textBox1_TypeAct.SelectedValue,Convert.ToInt32(textBox1_nbtotal.Text));
        }

        /// <summary>
        /// Appel de la fonction appelant la requete SQL qui réalisera la tache
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_suppr_Click(object sender, EventArgs e)
        {
            this.oEquipement.m_SupprimerEquipement(this.textBox1_Nom.Text);
        }

        /// <summary>
        /// Appel de la fonction appelant la requete SQL qui réalisera la tache
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Update_Click(object sender, EventArgs e)
        {
            try
            {
                this.oEquipement.m_MettreAjourEquipement(this.textBox1_Nom.Text, this.textBox1_descript.Text, this.textBox1_Puissance.Text, Convert.ToInt32(this.textBox1_dispo.Text), Convert.ToInt32(this.textBox1_cout.Text), Convert.ToInt32(this.textBox1_TypeAct.SelectedValue), Convert.ToInt32(textBox1_nbtotal.Text));
            }
            catch
            {
                MessageBox.Show("Une erreur est survenue\n"
                    + "Aucune information n'a été modifiée\n"
                    + "Merci de remplir toutes les boites de texte, même avec un espace", 
                    "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        /// <summary>
        /// Au chargement rempli les boites de texte déroulantes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void form_AjoutSupprModifEquipement_Load(object sender, EventArgs e)
        {
            textBox1_Nom.DataSource = oEquipement.m_GetAllEquipmentsData("rwsequip").Tables["rwsequip"];
            textBox1_Nom.DisplayMember = "Nom";
            textBox1_TypeAct.DataSource = oEquipement.m_GetActivités("rws").Tables["rws"];
            textBox1_TypeAct.DisplayMember = "Nom Type";
            textBox1_TypeAct.ValueMember = "N° Type";



        }

        /// <summary>
        /// fermeture du formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_close_Click(object sender, EventArgs e)
        {
            Close();
        }
        /// <summary>
        /// Affichafge des infos concernant un matériel
        /// Vidage des boites de texte avant affichage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox1_Nom_SelectionChangeCommitted(object sender, EventArgs e)
        {
            textBox1_cout.Text = "";
            textBox1_descript.Text = "";
            textBox1_dispo.Text = "";
            textBox1_Puissance.Text = "";
            textBox1_nbtotal.Text = "";
            textBox1_TypeAct.SelectedIndex = -1;
            DataSet Set = oEquipement.m_GetOneEquipement(textBox1_Nom.Text, "rwseqmptdata");
            try
            {
                textBox1_cout.Text = Convert.ToString((double)Set.Tables["rwseqmptdata"].Rows[0][7]);
            }
            catch {}
            try
            {
                textBox1_descript.Text = (string)Set.Tables["rwseqmptdata"].Rows[0][3];
            }
            catch {}
            try
            {
                textBox1_dispo.Text = Convert.ToString((int)Set.Tables["rwseqmptdata"].Rows[0][5]);
            }
            catch
            {}
            try
            {
                textBox1_Puissance.Text = (string)Set.Tables["rwseqmptdata"].Rows[0][4];
            }
            catch {}
            try
            {
                textBox1_TypeAct.SelectedIndex = ((int)Set.Tables["rwseqmptdata"].Rows[0][8])-1;
            }
            catch {}
            try
            {
                textBox1_nbtotal.Text = Convert.ToString((int)Set.Tables["rwseqmptdata"].Rows[0][6]);
            }
            catch{}
        }
    }
}
