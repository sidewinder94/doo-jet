﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Projet_Csharp.Couche_Client
{

    /// <summary>
    /// Class permettant d'afficher les informations concernant toutes les commandes
    /// </summary>
    public partial class form_AfficherReservations : Form
    {
        private Couche_Métier.Pcs_Reservations oReservations;
        /// <summary>
        /// Constructeur par défaut
        /// Initialise les objets
        /// </summary>
        public form_AfficherReservations()
        {
            oReservations = new Couche_Métier.Pcs_Reservations();
            InitializeComponent();
        }
        /// <summary>
        /// Ferme le formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exi_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Au chargement du formulaire charge les informations dans le datagridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void form_AfficherReservations_Load(object sender, EventArgs e)
        {
            dataGridView1.DataSource = oReservations.m_GetAllData("rows").Tables["rows"];
        }
    }
}
