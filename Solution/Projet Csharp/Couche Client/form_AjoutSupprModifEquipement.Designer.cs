﻿namespace Projet_Csharp.Couche_Client
{
    partial class form_AjoutSupprModifEquipement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1_add = new System.Windows.Forms.Button();
            this.button1_Update = new System.Windows.Forms.Button();
            this.button1_suppr = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1_descript = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1_Puissance = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox1_dispo = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1_cout = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox1_Nom = new System.Windows.Forms.ComboBox();
            this.button1_close = new System.Windows.Forms.Button();
            this.textBox1_TypeAct = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBox1_nbtotal = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button1_add
            // 
            this.button1_add.Location = new System.Drawing.Point(220, 129);
            this.button1_add.Name = "button1_add";
            this.button1_add.Size = new System.Drawing.Size(75, 23);
            this.button1_add.TabIndex = 0;
            this.button1_add.Text = "Ajout";
            this.button1_add.UseVisualStyleBackColor = true;
            this.button1_add.Click += new System.EventHandler(this.button1_add_Click);
            // 
            // button1_Update
            // 
            this.button1_Update.Location = new System.Drawing.Point(220, 155);
            this.button1_Update.Name = "button1_Update";
            this.button1_Update.Size = new System.Drawing.Size(75, 23);
            this.button1_Update.TabIndex = 1;
            this.button1_Update.Text = "Mettre à jour";
            this.button1_Update.UseVisualStyleBackColor = true;
            this.button1_Update.Click += new System.EventHandler(this.button1_Update_Click);
            // 
            // button1_suppr
            // 
            this.button1_suppr.Location = new System.Drawing.Point(220, 181);
            this.button1_suppr.Name = "button1_suppr";
            this.button1_suppr.Size = new System.Drawing.Size(75, 23);
            this.button1_suppr.TabIndex = 2;
            this.button1_suppr.Text = "Supprimer";
            this.button1_suppr.UseVisualStyleBackColor = true;
            this.button1_suppr.Click += new System.EventHandler(this.button1_suppr_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Nom";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Descriptif";
            // 
            // textBox1_descript
            // 
            this.textBox1_descript.Location = new System.Drawing.Point(114, 45);
            this.textBox1_descript.Name = "textBox1_descript";
            this.textBox1_descript.Size = new System.Drawing.Size(100, 20);
            this.textBox1_descript.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Puissance";
            // 
            // textBox1_Puissance
            // 
            this.textBox1_Puissance.Location = new System.Drawing.Point(114, 74);
            this.textBox1_Puissance.Name = "textBox1_Puissance";
            this.textBox1_Puissance.Size = new System.Drawing.Size(100, 20);
            this.textBox1_Puissance.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Disponibilité";
            // 
            // textBox1_dispo
            // 
            this.textBox1_dispo.Location = new System.Drawing.Point(114, 103);
            this.textBox1_dispo.Name = "textBox1_dispo";
            this.textBox1_dispo.Size = new System.Drawing.Size(100, 20);
            this.textBox1_dispo.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 158);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(91, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Coût HT Location";
            // 
            // textBox1_cout
            // 
            this.textBox1_cout.Location = new System.Drawing.Point(114, 155);
            this.textBox1_cout.Name = "textBox1_cout";
            this.textBox1_cout.Size = new System.Drawing.Size(100, 20);
            this.textBox1_cout.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 188);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Type d\'Activité";
            // 
            // textBox1_Nom
            // 
            this.textBox1_Nom.FormattingEnabled = true;
            this.textBox1_Nom.Location = new System.Drawing.Point(114, 19);
            this.textBox1_Nom.Name = "textBox1_Nom";
            this.textBox1_Nom.Size = new System.Drawing.Size(100, 21);
            this.textBox1_Nom.TabIndex = 15;
            this.textBox1_Nom.SelectedValueChanged += new System.EventHandler(this.textBox1_Nom_SelectionChangeCommitted);
            // 
            // button1_close
            // 
            this.button1_close.Location = new System.Drawing.Point(15, 208);
            this.button1_close.Name = "button1_close";
            this.button1_close.Size = new System.Drawing.Size(75, 23);
            this.button1_close.TabIndex = 17;
            this.button1_close.Text = "Retour";
            this.button1_close.UseVisualStyleBackColor = true;
            this.button1_close.Click += new System.EventHandler(this.button1_close_Click);
            // 
            // textBox1_TypeAct
            // 
            this.textBox1_TypeAct.FormattingEnabled = true;
            this.textBox1_TypeAct.Location = new System.Drawing.Point(114, 181);
            this.textBox1_TypeAct.Name = "textBox1_TypeAct";
            this.textBox1_TypeAct.Size = new System.Drawing.Size(100, 21);
            this.textBox1_TypeAct.TabIndex = 18;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 132);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "Nombre Total";
            // 
            // textBox1_nbtotal
            // 
            this.textBox1_nbtotal.Location = new System.Drawing.Point(114, 129);
            this.textBox1_nbtotal.Name = "textBox1_nbtotal";
            this.textBox1_nbtotal.Size = new System.Drawing.Size(100, 20);
            this.textBox1_nbtotal.TabIndex = 20;
            // 
            // form_AjoutSupprModifEquipement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 238);
            this.Controls.Add(this.textBox1_nbtotal);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBox1_TypeAct);
            this.Controls.Add(this.button1_close);
            this.Controls.Add(this.textBox1_Nom);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBox1_cout);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBox1_dispo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox1_Puissance);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBox1_descript);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1_suppr);
            this.Controls.Add(this.button1_Update);
            this.Controls.Add(this.button1_add);
            this.Name = "form_AjoutSupprModifEquipement";
            this.Text = "Ajout/Suppression/Modification d\'un équipement";
            this.Load += new System.EventHandler(this.form_AjoutSupprModifEquipement_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1_add;
        private System.Windows.Forms.Button button1_Update;
        private System.Windows.Forms.Button button1_suppr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1_descript;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1_Puissance;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBox1_dispo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox1_cout;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox textBox1_Nom;
        private System.Windows.Forms.Button button1_close;
        private System.Windows.Forms.ComboBox textBox1_TypeAct;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox1_nbtotal;
    }
}