﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Drawing.Printing;

namespace Projet_Csharp.Couche_Client
{
    /// <summary>
    /// Formulaire de réservations
    /// </summary>
    public partial class form_Reservation : Form
    {
        /// <summary>
        /// création des objets
        /// </summary>
        private Font printFont;
        private StreamReader streamToPrint;
        private Couche_Métier.Pcs_Impression oImpression;
        private Couche_Métier.Pcs_Reservations oReservations;
        private Couche_Métier.Pcs_Equipement oEquipement;
        private Couche_Métier.Pcs_Effectifs oEffectifs;

        public form_Reservation()
        {
            InitializeComponent();
            oReservations = new Couche_Métier.Pcs_Reservations();
            oEquipement = new Couche_Métier.Pcs_Equipement();
            oEffectifs = new Couche_Métier.Pcs_Effectifs();
            oImpression = new Couche_Métier.Pcs_Impression();
        }

        /// <summary>
        /// Initialisation des valeurs des controles du formulaire
        /// tests
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void form_Reservation_Load(object sender, EventArgs e)
        {

            DataSet temp = oReservations.m_GetAllCustomersName("custname");
            CustomerLastName_comboBox.DataSource = temp.Tables["custname"];
            CustomerLastName_comboBox.DisplayMember = "Nom";
            temp = oEquipement.m_GetAllEquipments("eqpmtname");
            Equipment_combobox.DataSource = temp.Tables["eqpmtname"];
            Equipment_combobox.DisplayMember = "Nom";
            CommandChoice_comboBox.DataSource = oReservations.m_GetAll("rows").Tables["rows"];
            CommandChoice_comboBox.DisplayMember = "Reservation";


            temp = oEffectifs.m_GetNameByDisponibility(BeginDay_maskedTextBox.Text,
               BeginHour_maskedTextBox.Text, EndDay_maskedTextBox.Text,
               EndHour_maskedTextBox.Text, "slrydispo");
            SalaryLastName_comboBox.DataSource = temp.Tables["slrydispo"];
            SalaryLastName_comboBox.DisplayMember = "Nom";

        }

        /// <summary>
        /// Ajouter la réservation
        /// Afficher les nom des moniteurs disponibles
        /// Indiquer la réservation ajoutée comme étant la reservation active
        /// pour l'ajout de matériel à une réservation
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Next_button_Click(object sender, EventArgs e)
        {
            tabControl1.SelectTab(1);

            /*Ajouter la réservation*/
            oReservations.m_AddReservation(BeginDay_maskedTextBox.Text,
                BeginHour_maskedTextBox.Text, EndDay_maskedTextBox.Text,
                EndHour_maskedTextBox.Text, CustomerFirstName_combobox.Text,
                CustomerLastName_comboBox.Text);

            /*Afficher les nom des moniteurs disponibles*/
            DataSet temp = oEffectifs.m_GetNameByDisponibility(BeginDay_maskedTextBox.Text,
                BeginHour_maskedTextBox.Text, EndDay_maskedTextBox.Text,
                EndHour_maskedTextBox.Text, "slrydispo");
            SalaryLastName_comboBox.DataSource = temp.Tables["slrydispo"];
            SalaryLastName_comboBox.DisplayMember = "Nom";

            /*Indiquer la réservation ajoutée comme étant la reservation active
             *pour l'ajout de matériel à une réservation*/
            CommandChoice_comboBox.DataSource = oReservations.m_GetAll("rows").Tables["rows"];
            CommandChoice_comboBox.DisplayMember = "Reservation";
            CommandChoice_comboBox.ValueMember = "Reservation";
            int last = oReservations.m_GetLast();
            CommandChoice_comboBox.SelectedValue = last;
        }

        /// <summary>
        /// Remplissage automatique
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CustomerFirstName_combobox_Click(object sender, EventArgs e)
        {
            DataSet temp = oReservations.
                m_GetCustomerByName(CustomerLastName_comboBox.Text, "custfrsname");
            CustomerFirstName_combobox.DataSource = temp.Tables["custfrsname"];
            CustomerFirstName_combobox.DisplayMember = "Prénom";
        }

        /// <summary>
        /// Remplissage de la boite de texte déroulante présentant la dispobibilité d'un équipement
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void equipmentDisponibility_comboBox_Click(object sender, EventArgs e)
        {
            equipmentDisponibility_comboBox.Items.Clear();
            int max = oEquipement.m_GetDisponibilityByName(Equipment_combobox.Text);
            for (int i = 0; i <= max; i++)
            {
                equipmentDisponibility_comboBox.Items.Add(i);
            }
        }
        /// <summary>
        /// Présente le cout en fonction du nombre réservé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void equipmentDisponibility_comboBox_ValueChanged(object sender, EventArgs e)
        {
            Unit_Cost_Modifiable_Label.Text = oEquipement.m_GetPrice(Equipment_combobox.Text,
                equipmentDisponibility_comboBox.Text);
        }

        /// <summary>
        /// remplissage auto
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SalaryFirstName_comboBox_Click(object sender, EventArgs e)
        {
            DataSet temp = oEffectifs.m_GetFirstNameByDisponibilityAndName(BeginDay_maskedTextBox.Text,
            BeginHour_maskedTextBox.Text, EndDay_maskedTextBox.Text,
            EndHour_maskedTextBox.Text, SalaryLastName_comboBox.Text, "slrydispofirstname");
            SalaryFirstName_comboBox.DataSource = temp.Tables["slrydispofirstname"];
            SalaryFirstName_comboBox.DisplayMember = "Prénom";
        }

        /// <summary>
        /// /*Appel du formulaire de vérification des matériels*/
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckMaterial_button_Click(object sender, EventArgs e)
        {

            Couche_Client.form_AfficherEquipement form = new form_AfficherEquipement(Equipment_combobox.Text);
            form.ShowDialog(this);

        }

        /// <summary>
        /// /*Fonction d'ajout du matériel à une commande*/
        /// /*Fonction de mise a jour de Label total de Commande et Maj cout total de comande*/
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddMaterial_button_Click(object sender, EventArgs e)
        {
            oEquipement.m_AddEquimpentByName(Convert.ToInt32(CommandChoice_comboBox.Text),
                Equipment_combobox.Text, Convert.ToInt32(equipmentDisponibility_comboBox.Text),
                Convert.ToDouble(Unit_Cost_Modifiable_Label.Text));

            TotalPriceModifiable_label.Text = Convert.ToString(oReservations.m_GetPriceByID(Convert.ToInt32(CommandChoice_comboBox.Text)));
        }

        /// <summary>
        /// appel de la fonction puis requete permattant de réaliser l'action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddMonitor_button_Click(object sender, EventArgs e)
        {
            oEffectifs.m_AddToReservation(SalaryLastName_comboBox.Text,
                SalaryFirstName_comboBox.Text, CommandChoice_comboBox.Text);
        }

        /// <summary>
        /// changement d'onglet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void previous_button_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 0;
        }

        /// <summary>
        /// changement d'onglet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void next_button2_Click(object sender, EventArgs e)
        {
            tabControl1.SelectedIndex = 2;
            CommandReport_richTextBox.Text = oReservations.m_MakeReport(CommandChoice_comboBox.Text);
        }

        /// <summary>
        /// appel de la fonction puis requete permattant de réaliser l'action
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DeleteMaterial_button_Click(object sender, EventArgs e)
        {
            Couche_Client.form_SupressionActivité oSuppression = new form_SupressionActivité(Convert.ToInt32(CommandChoice_comboBox.Text));
            oSuppression.ShowDialog(this);
        }
        /// <summary>
        /// fermeture du formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_close_Click(object sender, EventArgs e)
        {
            Close();
        }
        /// <summary>
        /// fermeture du formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button7_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// Affichage d'une boite de dialoque permettant le choix de l'imprimante
        /// Selection de la police
        /// Affichage d'une boite de dialogue pour sélectionner l'imprimante
        /// Lancement de l'impression
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void print_button_Click(object sender, EventArgs e)
        {
            DialogResult result;
            printDialog.PrintToFile = false;
            result = printDialog.ShowDialog(this);

            if (result == DialogResult.OK)
            {                
                streamToPrint = new StreamReader(oImpression.GetTempFile(CommandReport_richTextBox.Text));
                printFont = new Font("Arial", 12);
                printDocument1.PrinterSettings = printDialog.PrinterSettings;
                printDocument1.Print();
            }
            else if (result == DialogResult.Cancel)
            {
                MessageBox.Show("L'impression a été annulée", "Impression annulée",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        /// <summary>
        /// fonction appellée lors de l'impression d'une page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ev"></param>
        private void printDocument1_PrintPage(object sender, PrintPageEventArgs ev)
        {
            /*Definition de variables*/
            float linesPerPage = 0;
            float yPos = 0;
            int count = 0;
            float leftMargin = ev.MarginBounds.Left;
            float topMargin = ev.MarginBounds.Top;
            string line = null;

            // Calcul du nombre de lignes par page
            linesPerPage = ev.MarginBounds.Height / 
                printFont.GetHeight(ev.Graphics);

            // Imprime chaque ligne du fichier
            while (count < linesPerPage &&
               ((line = streamToPrint.ReadLine()) != null))
            {
                yPos = topMargin + (count *
                   printFont.GetHeight(ev.Graphics));
                ev.Graphics.DrawString(line, printFont, Brushes.Black,
                   leftMargin, yPos, new StringFormat());
                count++;
            }

            // Si il existe plus de lignes imprimer une nouvelle page
            if (line != null)
                ev.HasMorePages = true;
            else
                ev.HasMorePages = false; 
        }
    }
}
