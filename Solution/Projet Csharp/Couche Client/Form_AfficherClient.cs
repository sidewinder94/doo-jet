﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Projet_Csharp.Couche_Client
{
    /// <summary>
    /// Class Permettant d'afficher les détails d'un client
    /// </summary>
    public partial class Form_AfficherClient : Form
    {
        /// <summary>
        /// Création de l'objet Data et Client
        /// </summary>
        private System.Data.DataSet oDATASET3;
        private Projet_Csharp.Couche_Métier.Client oClient;

        /// <summary>
        /// Constructeur, initialise les objets utilisés
        /// </summary>
        public Form_AfficherClient()
        {
            this.oDATASET3 = new DataSet();
            this.oClient = new Projet_Csharp.Couche_Métier.Client();
            InitializeComponent();
        }
        /// <summary>
        /// Événement déclenché au chargement de la form, rempli la combobox Nom
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_AfficherClient_Load(object sender, EventArgs e)
        {
            this.nom_comboBox.DataSource = oClient.action_AfficherTout("rwsname").Tables["rwsname"];
            this.nom_comboBox.DisplayMember = "Nom";
        }
        /// <summary>
        /// remplissage de la combobox prénom au clic
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void prenom_combobox_Click(object sender, EventArgs e)
        {
            this.prenom_combobox.DataSource = oClient.action_AfficherPrenom(nom_comboBox.Text,
                "rwsfrstname").Tables["rwsfrstname"];
            this.prenom_combobox.DisplayMember = "Prénom";
        }
        /// <summary>
        /// Apres modification de la combobox prénom, affiche dans le datagridview
        /// Les informations correspondantes
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void prenom_combobox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            this.dataGridView1.DataSource = oClient.action_AfficherClient(nom_comboBox.Text, prenom_combobox.Text, "rwscmplete").Tables["rwscmplete"];
        }
        /// <summary>
        /// Ferme le formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }


    }
}
