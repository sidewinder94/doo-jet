﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Projet_Csharp.Couche_Client
{
    /// <summary>
    /// Permet d'afficher le détail d'un effectif, de le modifier et de le supprimer
    /// </summary>
    public partial class form_AfficherUnEffectif_Modif : Form
    {
        /// <summary>
        /// création des objets
        /// </summary>
        private System.Data.DataSet oDATASET3;
        private Projet_Csharp.Couche_Métier.Pcs_Effectifs oEffectif;
        private Couche_Métier.Pcs_Contrats oContrats;
        /// <summary>
        /// Constructeur par défaut
        /// Initialise les objets utilisés
        /// </summary>
        public form_AfficherUnEffectif_Modif()
        {
            this.oDATASET3 = new DataSet();
            this.oEffectif = new Projet_Csharp.Couche_Métier.Pcs_Effectifs();
            this.oContrats = new Couche_Métier.Pcs_Contrats();
            InitializeComponent();
        }

        /// <summary>
        /// Appel de la fonction et requete SQL pour effectuer la recherche et remplir les textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_trouver_Click(object sender, EventArgs e)
        {
            this.oDATASET3 = this.oEffectif.m_afficherUnEffectif(this.textBox1_Nom.Text, this.FirstName_comboBox.Text, "rwsEffectif");
            this.textBox1_Secu.Text = (string)oDATASET3.Tables["rwsEffectif"].Rows[0][1];
            this.textBox2_DateEmb.Text = (string)oDATASET3.Tables["rwsEffectif"].Rows[0][2];
            this.textBox3_DateVMedic.Text = (string)oDATASET3.Tables["rwsEffectif"].Rows[0][3];
            this.textBox4_Contrat.SelectedIndex = ((int)oDATASET3.Tables["rwsEffectif"].Rows[0][4]) - 1;
            this.textBox5_PermisCotier.Text = (string)oDATASET3.Tables["rwsEffectif"].Rows[0][5];
            this.textBox6_BEES.Text = (string)oDATASET3.Tables["rwsEffectif"].Rows[0][6];
            this.comboBox1_Statut.Text = (string)oDATASET3.Tables["rwsEffectif"].Rows[0][7];
            this.textBox8_Prenom.Text = (string)oDATASET3.Tables["rwsEffectif"].Rows[0][8];
            this.textBox9_nom.Text = (string)oDATASET3.Tables["rwsEffectif"].Rows[0][9];
        }

        /// <summary>
        /// Appel de la fonction puis de la requete SQL pour supprimer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_suppr_Click(object sender, EventArgs e)
        {
            this.oEffectif.m_supprimerEffectif(this.textBox1_Nom.Text);
            actualiser_informations();

        }

        /// <summary>
        /// Appel de la fonction puis de la requete SQL pour Mettre à jour
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_MiseàJ_Click(object sender, EventArgs e)
        {
            int ID = oEffectif.m_GetID(textBox1_Nom.Text, FirstName_comboBox.Text);
            this.oEffectif.m_MettreAjourEffectif(this.textBox1_Secu.Text,this.textBox2_DateEmb.Text, this.textBox3_DateVMedic.Text, (int)(textBox4_Contrat.SelectedValue), this.textBox5_PermisCotier.Text,this.textBox6_BEES.Text,this.comboBox1_Statut.Text, this.textBox8_Prenom.Text, this.textBox9_nom.Text, ID);
        }

        /// <summary>
        /// Chargement des informations dans la boite de texte
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void form_AfficherUnEffectif_Modif_Load(object sender, EventArgs e)
        {
            actualiser_informations();            
        }
        /// <summary>
        /// fermeture du formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_close_Click(object sender, EventArgs e)
        {
           
            this.Close();
        }

        /// <summary>
        /// affichage du prénom dans la combobox via une requete et fonction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FirstName_Combobox_Click(object sender, EventArgs e)
        {
            FirstName_comboBox.DataSource = oEffectif.m_GetFirstNameByName(textBox1_Nom.Text, "rowsname").Tables["rowsname"];
            FirstName_comboBox.DisplayMember = "Prénom";
        }

        /// <summary>
        /// Si plus de 10 caractère dans la TextBoxBEES alors Moniteur JetSki s'affiche dans la comboBox Activité.
        /// Avec plusieurs tests après le changement du nombre de caractère.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_Statut_Click(object sender, EventArgs e)
        {
            if (textBox6_BEES.Text.Length >= 10)
            {
                if (comboBox1_Statut.Items.Contains("Moniteur JetSki"))
                {
                }
                else
                {
                    comboBox1_Statut.Items.Add("Moniteur JetSki");
                }
            }
            else
            {
                if (comboBox1_Statut.Items.Contains("Moniteur JetSki"))
                {
                    comboBox1_Statut.Items.Remove("Moniteur JetSki");
                }
            }
        }
        private void actualiser_informations()
        {
            textBox1_Nom.DataSource = oEffectif.m_GetAllNames("rowsname").Tables["rowsname"];
            textBox1_Nom.DisplayMember = "Nom";
            DataSet temp = oContrats.m_GetContrats("rowsname");
            this.textBox4_Contrat.DataSource = temp.Tables["rowsname"];
            this.textBox4_Contrat.ValueMember = "ID_Contrat";
            this.textBox4_Contrat.DisplayMember = "Type Contrat";
        }
    }
}
