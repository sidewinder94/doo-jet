﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Projet_Csharp.Couche_Client
{
    /// <summary>
    /// Affiche le nom et N° de série de tous les équipements
    /// </summary>
    public partial class form_AfficherToutEquipement : Form
    {
        /// <summary>
        /// création des objets
        /// </summary>
        private System.Data.DataSet oDATASET;
        private Projet_Csharp.Couche_Métier.Pcs_Equipement oEquipement;
        /// <summary>
        /// Constructeur par défaut initialise les objets
        /// </summary>
        public form_AfficherToutEquipement()
        {
            this.oDATASET = new DataSet();
            this.oEquipement = new Projet_Csharp.Couche_Métier.Pcs_Equipement();
            InitializeComponent();
        }

        /// <summary>
        /// affichage des données dans la GridView par l'appel de la fonction et de la requete SQL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void form_AfficherToutEquipement_Load(object sender, EventArgs e)
        {
            this.oDATASET = this.oEquipement.m_GetAllEquipments("rwsEquipement");
            this.dataGridView1.DataSource = this.oDATASET.Tables["rwsEquipement"];
        }

        /// <summary>
        /// fermeture du formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_close_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
