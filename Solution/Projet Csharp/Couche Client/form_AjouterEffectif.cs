﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Projet_Csharp.Couche_Client
{
    /// <summary>
    /// Formulaire d'ajout d'effectif
    /// </summary>
    public partial class form_AjouterEffectif : Form
    {
        private Projet_Csharp.Couche_Métier.Pcs_Effectifs oEffectif;
        private Couche_Métier.Pcs_Contrats oContrat;

        /// <summary>
        /// création de l'objet
        /// </summary>
        public form_AjouterEffectif()
        {
            this.oEffectif = new Projet_Csharp.Couche_Métier.Pcs_Effectifs();
            this.oContrat = new Couche_Métier.Pcs_Contrats();
            InitializeComponent();
        }
        /// <summary>
        /// Au chrgement, remplir la combobox représentant les contrats disponibles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void form_AjouterEffectif_Load(object sender, EventArgs e)
        {
            textBox4_Contrat.DataSource = oContrat.m_GetContrats("rws").Tables["rws"];
            textBox4_Contrat.DisplayMember = "Type Contrat";
            textBox4_Contrat.ValueMember = "ID_Contrat";
        }

        /// <summary>
        /// Appel de la fonction puis requete permettant l'ajout d'un effectif
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_add_Click(object sender, EventArgs e)
        {
            this.oEffectif.m_ajouterEffectif(this.textBox1_Secu.Text, this.textBox2_DateEmb.Text, this.textBox3_DateVMedic.Text, Convert.ToInt32((int)this.textBox4_Contrat.SelectedValue), this.textBox5_PermisCotier.Text, this.textBox6_BEES.Text, this.comboBox1_Statut.Text, this.textBox8_Prenom.Text, this.textBox9_Nom.Text);
            this.Close();
        }

        /// <summary>
        /// Moniteur JetSki ne s'affiche que si 10 chiffres sont écrit dans la TextBoxBEES
        /// Sinon supprimer de Moniteur JetSKi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            if (textBox6_BEES.Text.Length >= 10)
            {
                comboBox1_Statut.Items.Add("Moniteur JetSki");
            }
            else
            {
                try
                {
                    comboBox1_Statut.Items.Remove("Moniteur JetSki");
                }
                catch
                {
                }
            }
        }

        /// <summary>
        /// fermeture du formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_close_Click(object sender, EventArgs e)
        {
            Close();
        }


    }
}
