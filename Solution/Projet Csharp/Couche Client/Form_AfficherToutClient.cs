﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Projet_Csharp.Couche_Client
{
    /// <summary>
    /// Form permettant l'affichage des noms et prénoms de tous les clients
    /// </summary>
    public partial class Form_AfficherToutClient : Form
    {
        /// <summary>
        /// création d'objet Data et client
        /// </summary>
        private System.Data.DataSet oDATASET2;
        private Projet_Csharp.Couche_Métier.Client oClient;
        /// <summary>
        /// Constructeur par défaut
        /// Initialisation de tous les objets
        /// </summary>
        public Form_AfficherToutClient()
        {
            this.oDATASET2 = new DataSet();
            this.oClient = new Projet_Csharp.Couche_Métier.Client();
            InitializeComponent();
        }
        /// <summary>
        /// fermeture du formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_close_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        /// affichage des données dans la GridView par l'appel de la fonction et de la requete SQL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_AfficherToutClient_Load(object sender, EventArgs e)
        {
            this.oDATASET2 = this.oClient.action_AfficherTout("rowsClient");
            this.dataGridView1.DataSource = this.oDATASET2.Tables["rowsClient"];
        }
    }
}
