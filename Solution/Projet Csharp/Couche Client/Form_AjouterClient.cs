﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Projet_Csharp.Couche_Client
{
    /// <summary>
    /// Formulaire d'ajout de client
    /// </summary>
    public partial class Form_AjouterClient : Form
    {
        private Projet_Csharp.Couche_Métier.Client oClient;

        /// <summary>
        /// création de l'objet client
        /// </summary>
        public Form_AjouterClient()
        {
            this.oClient = new Projet_Csharp.Couche_Métier.Client();
            InitializeComponent();
        }

        /// <summary>
        /// Afficher les infos dans les tables directement.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form_AjouterClient_Load(object sender, EventArgs e)
        {
            actualisation();
        }

        /// <summary>
        /// appel de la fonction et requete qui permet la création d'un client.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            this.oClient.action_AjouterClient(this.comboBox1.Text, this.comboBox2.Text, this.textBox4.Text, this.textBox3.Text, this.textBox5.Text, this.textBox6.Text);
            this.Close();
        }
        /// <summary>
        /// fermeture du formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button2_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        /// <summary>
        /// appel de la fonction et requete qui permet la modification d'un client.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button3_Click(object sender, EventArgs e)
        {
            this.oClient.action_ModifierClient(this.comboBox1.Text, this.comboBox2.Text, this.textBox4.Text, this.textBox3.Text, this.textBox5.Text, this.textBox6.Text);
            this.Close();

        }

        /// <summary>
        /// appel de la fonction et requete permettant la suppression d'un client.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button4_Click(object sender, EventArgs e)
        {
            this.oClient.action_SupprimerClient(this.comboBox2.Text);
            this.Close();
        }
        /// <summary>
        /// Remplissage de la combobox sur clic
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_Click(object sender, EventArgs e)
        {
            DataSet test = oClient.action_AfficherPrenom(comboBox2.Text, "rows");
            comboBox1.DataSource = test.Tables["rows"];
            comboBox1.DisplayMember = "Prénom";
        }
        /// <summary>
        /// Fonction d'actualisation
        /// </summary>
        private void actualisation()
        {
            comboBox2.DataSource = oClient.action_AfficherTout("rows").Tables["rows"];
            comboBox2.DisplayMember = "Nom";
        }
        /// <summary>
        /// Affichae des données Clients dès lors qu'un prénom à été sélectionné
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_SelectionChangeCommitted(object sender, EventArgs e)
        {
            DataSet Set = oClient.action_AfficherClient(comboBox2.Text, comboBox1.Text, "rowsclient");
            try
            {
                textBox3.Text = (string)Set.Tables["rowsclient"].Rows[0][4];
            }
            catch { }
            try
            {
                textBox4.Text = (string)Set.Tables["rowsclient"].Rows[0][3];
            }
            catch { }
            try
            {
                textBox5.Text = (string)Set.Tables["rowsclient"].Rows[0][5];
            }
            catch { }
            try
            {
                textBox6.Text = (string)Set.Tables["rowsclient"].Rows[0][6];
            }
            catch { }
        }
    }
}
