﻿namespace Projet_Csharp.Couche_Client
{
    partial class form_Reservation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button1_close = new System.Windows.Forms.Button();
            this.Next_button = new System.Windows.Forms.Button();
            this.CustomerFirstName_combobox = new System.Windows.Forms.ComboBox();
            this.CustomerLastName_comboBox = new System.Windows.Forms.ComboBox();
            this.EndHour_maskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.EndDay_maskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.BeginHour_maskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.BeginDay_maskedTextBox = new System.Windows.Forms.MaskedTextBox();
            this.Client_label = new System.Windows.Forms.Label();
            this.EndHour_label = new System.Windows.Forms.Label();
            this.EndDay_label = new System.Windows.Forms.Label();
            this.BeginHour_Label = new System.Windows.Forms.Label();
            this.BeginDay_label = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.AddMonitor_button = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.TotalPriceModifiable_label = new System.Windows.Forms.Label();
            this.Unit_Cost_Modifiable_Label = new System.Windows.Forms.Label();
            this.CommandChoice_comboBox = new System.Windows.Forms.ComboBox();
            this.DeleteMaterial_button = new System.Windows.Forms.Button();
            this.TotalPrice_label = new System.Windows.Forms.Label();
            this.Unit_Cost_label = new System.Windows.Forms.Label();
            this.AddMaterial_button = new System.Windows.Forms.Button();
            this.previous_button = new System.Windows.Forms.Button();
            this.next_button2 = new System.Windows.Forms.Button();
            this.CheckMaterial_button = new System.Windows.Forms.Button();
            this.equipmentDisponibility_comboBox = new System.Windows.Forms.ComboBox();
            this.SalaryFirstName_comboBox = new System.Windows.Forms.ComboBox();
            this.SalaryLastName_comboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.Equipment_combobox = new System.Windows.Forms.ComboBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button7 = new System.Windows.Forms.Button();
            this.CommandReport_richTextBox = new System.Windows.Forms.RichTextBox();
            this.printDialog = new System.Windows.Forms.PrintDialog();
            this.print_button = new System.Windows.Forms.Button();
            this.printDocument1 = new System.Drawing.Printing.PrintDocument();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(486, 264);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button1_close);
            this.tabPage1.Controls.Add(this.Next_button);
            this.tabPage1.Controls.Add(this.CustomerFirstName_combobox);
            this.tabPage1.Controls.Add(this.CustomerLastName_comboBox);
            this.tabPage1.Controls.Add(this.EndHour_maskedTextBox);
            this.tabPage1.Controls.Add(this.EndDay_maskedTextBox);
            this.tabPage1.Controls.Add(this.BeginHour_maskedTextBox);
            this.tabPage1.Controls.Add(this.BeginDay_maskedTextBox);
            this.tabPage1.Controls.Add(this.Client_label);
            this.tabPage1.Controls.Add(this.EndHour_label);
            this.tabPage1.Controls.Add(this.EndDay_label);
            this.tabPage1.Controls.Add(this.BeginHour_Label);
            this.tabPage1.Controls.Add(this.BeginDay_label);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(478, 238);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Créer la réservation";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button1_close
            // 
            this.button1_close.Location = new System.Drawing.Point(11, 209);
            this.button1_close.Name = "button1_close";
            this.button1_close.Size = new System.Drawing.Size(75, 23);
            this.button1_close.TabIndex = 13;
            this.button1_close.Text = "Retour";
            this.button1_close.UseVisualStyleBackColor = true;
            this.button1_close.Click += new System.EventHandler(this.button1_close_Click);
            // 
            // Next_button
            // 
            this.Next_button.Location = new System.Drawing.Point(400, 212);
            this.Next_button.Name = "Next_button";
            this.Next_button.Size = new System.Drawing.Size(75, 23);
            this.Next_button.TabIndex = 12;
            this.Next_button.Text = "Suivant";
            this.Next_button.UseVisualStyleBackColor = true;
            this.Next_button.Click += new System.EventHandler(this.Next_button_Click);
            // 
            // CustomerFirstName_combobox
            // 
            this.CustomerFirstName_combobox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.CustomerFirstName_combobox.FormattingEnabled = true;
            this.CustomerFirstName_combobox.Location = new System.Drawing.Point(203, 110);
            this.CustomerFirstName_combobox.Name = "CustomerFirstName_combobox";
            this.CustomerFirstName_combobox.Size = new System.Drawing.Size(121, 21);
            this.CustomerFirstName_combobox.TabIndex = 11;
            this.CustomerFirstName_combobox.Text = "Prénom";
            this.CustomerFirstName_combobox.Click += new System.EventHandler(this.CustomerFirstName_combobox_Click);
            // 
            // CustomerLastName_comboBox
            // 
            this.CustomerLastName_comboBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.CustomerLastName_comboBox.FormattingEnabled = true;
            this.CustomerLastName_comboBox.Location = new System.Drawing.Point(76, 110);
            this.CustomerLastName_comboBox.Name = "CustomerLastName_comboBox";
            this.CustomerLastName_comboBox.Size = new System.Drawing.Size(121, 21);
            this.CustomerLastName_comboBox.TabIndex = 10;
            this.CustomerLastName_comboBox.Text = "Nom";
            // 
            // EndHour_maskedTextBox
            // 
            this.EndHour_maskedTextBox.Location = new System.Drawing.Point(76, 84);
            this.EndHour_maskedTextBox.Mask = "00:00";
            this.EndHour_maskedTextBox.Name = "EndHour_maskedTextBox";
            this.EndHour_maskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.EndHour_maskedTextBox.TabIndex = 9;
            this.EndHour_maskedTextBox.ValidatingType = typeof(System.DateTime);
            // 
            // EndDay_maskedTextBox
            // 
            this.EndDay_maskedTextBox.Location = new System.Drawing.Point(76, 58);
            this.EndDay_maskedTextBox.Mask = "00/00/0000";
            this.EndDay_maskedTextBox.Name = "EndDay_maskedTextBox";
            this.EndDay_maskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.EndDay_maskedTextBox.TabIndex = 8;
            this.EndDay_maskedTextBox.ValidatingType = typeof(System.DateTime);
            // 
            // BeginHour_maskedTextBox
            // 
            this.BeginHour_maskedTextBox.Location = new System.Drawing.Point(76, 32);
            this.BeginHour_maskedTextBox.Mask = "00:00";
            this.BeginHour_maskedTextBox.Name = "BeginHour_maskedTextBox";
            this.BeginHour_maskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.BeginHour_maskedTextBox.TabIndex = 7;
            this.BeginHour_maskedTextBox.ValidatingType = typeof(System.DateTime);
            // 
            // BeginDay_maskedTextBox
            // 
            this.BeginDay_maskedTextBox.Location = new System.Drawing.Point(76, 6);
            this.BeginDay_maskedTextBox.Mask = "00/00/0000";
            this.BeginDay_maskedTextBox.Name = "BeginDay_maskedTextBox";
            this.BeginDay_maskedTextBox.Size = new System.Drawing.Size(100, 20);
            this.BeginDay_maskedTextBox.TabIndex = 6;
            this.BeginDay_maskedTextBox.ValidatingType = typeof(System.DateTime);
            // 
            // Client_label
            // 
            this.Client_label.AutoSize = true;
            this.Client_label.Location = new System.Drawing.Point(8, 113);
            this.Client_label.Name = "Client_label";
            this.Client_label.Size = new System.Drawing.Size(33, 13);
            this.Client_label.TabIndex = 4;
            this.Client_label.Text = "Client";
            // 
            // EndHour_label
            // 
            this.EndHour_label.AutoSize = true;
            this.EndHour_label.Location = new System.Drawing.Point(8, 87);
            this.EndHour_label.Name = "EndHour_label";
            this.EndHour_label.Size = new System.Drawing.Size(53, 13);
            this.EndHour_label.TabIndex = 3;
            this.EndHour_label.Text = "Heure Fin";
            // 
            // EndDay_label
            // 
            this.EndDay_label.AutoSize = true;
            this.EndDay_label.Location = new System.Drawing.Point(8, 61);
            this.EndDay_label.Name = "EndDay_label";
            this.EndDay_label.Size = new System.Drawing.Size(47, 13);
            this.EndDay_label.TabIndex = 2;
            this.EndDay_label.Text = "Date Fin";
            // 
            // BeginHour_Label
            // 
            this.BeginHour_Label.AutoSize = true;
            this.BeginHour_Label.Location = new System.Drawing.Point(8, 35);
            this.BeginHour_Label.Name = "BeginHour_Label";
            this.BeginHour_Label.Size = new System.Drawing.Size(68, 13);
            this.BeginHour_Label.TabIndex = 1;
            this.BeginHour_Label.Text = "Heure Début";
            // 
            // BeginDay_label
            // 
            this.BeginDay_label.AutoSize = true;
            this.BeginDay_label.Location = new System.Drawing.Point(8, 9);
            this.BeginDay_label.Name = "BeginDay_label";
            this.BeginDay_label.Size = new System.Drawing.Size(62, 13);
            this.BeginDay_label.TabIndex = 0;
            this.BeginDay_label.Text = "Date Début";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.AddMonitor_button);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.TotalPriceModifiable_label);
            this.tabPage2.Controls.Add(this.Unit_Cost_Modifiable_Label);
            this.tabPage2.Controls.Add(this.CommandChoice_comboBox);
            this.tabPage2.Controls.Add(this.DeleteMaterial_button);
            this.tabPage2.Controls.Add(this.TotalPrice_label);
            this.tabPage2.Controls.Add(this.Unit_Cost_label);
            this.tabPage2.Controls.Add(this.AddMaterial_button);
            this.tabPage2.Controls.Add(this.previous_button);
            this.tabPage2.Controls.Add(this.next_button2);
            this.tabPage2.Controls.Add(this.CheckMaterial_button);
            this.tabPage2.Controls.Add(this.equipmentDisponibility_comboBox);
            this.tabPage2.Controls.Add(this.SalaryFirstName_comboBox);
            this.tabPage2.Controls.Add(this.SalaryLastName_comboBox);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Controls.Add(this.Equipment_combobox);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(478, 238);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Ajout d\'activités";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // AddMonitor_button
            // 
            this.AddMonitor_button.Location = new System.Drawing.Point(9, 175);
            this.AddMonitor_button.Name = "AddMonitor_button";
            this.AddMonitor_button.Size = new System.Drawing.Size(121, 23);
            this.AddMonitor_button.TabIndex = 17;
            this.AddMonitor_button.Text = "Ajouter un moniteur";
            this.AddMonitor_button.UseVisualStyleBackColor = true;
            this.AddMonitor_button.Click += new System.EventHandler(this.AddMonitor_button_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 13);
            this.label3.TabIndex = 16;
            this.label3.Text = "Commande active:";
            // 
            // TotalPriceModifiable_label
            // 
            this.TotalPriceModifiable_label.AutoSize = true;
            this.TotalPriceModifiable_label.Location = new System.Drawing.Point(69, 127);
            this.TotalPriceModifiable_label.Name = "TotalPriceModifiable_label";
            this.TotalPriceModifiable_label.Size = new System.Drawing.Size(0, 13);
            this.TotalPriceModifiable_label.TabIndex = 15;
            // 
            // Unit_Cost_Modifiable_Label
            // 
            this.Unit_Cost_Modifiable_Label.AutoSize = true;
            this.Unit_Cost_Modifiable_Label.Location = new System.Drawing.Point(334, 42);
            this.Unit_Cost_Modifiable_Label.Name = "Unit_Cost_Modifiable_Label";
            this.Unit_Cost_Modifiable_Label.Size = new System.Drawing.Size(0, 13);
            this.Unit_Cost_Modifiable_Label.TabIndex = 14;
            // 
            // CommandChoice_comboBox
            // 
            this.CommandChoice_comboBox.FormattingEnabled = true;
            this.CommandChoice_comboBox.Location = new System.Drawing.Point(107, 9);
            this.CommandChoice_comboBox.Name = "CommandChoice_comboBox";
            this.CommandChoice_comboBox.Size = new System.Drawing.Size(121, 21);
            this.CommandChoice_comboBox.TabIndex = 13;
            // 
            // DeleteMaterial_button
            // 
            this.DeleteMaterial_button.Location = new System.Drawing.Point(9, 95);
            this.DeleteMaterial_button.Name = "DeleteMaterial_button";
            this.DeleteMaterial_button.Size = new System.Drawing.Size(121, 23);
            this.DeleteMaterial_button.TabIndex = 12;
            this.DeleteMaterial_button.Text = "Supprimer une activité";
            this.DeleteMaterial_button.UseVisualStyleBackColor = true;
            this.DeleteMaterial_button.Click += new System.EventHandler(this.DeleteMaterial_button_Click);
            // 
            // TotalPrice_label
            // 
            this.TotalPrice_label.AutoSize = true;
            this.TotalPrice_label.Location = new System.Drawing.Point(6, 127);
            this.TotalPrice_label.Name = "TotalPrice_label";
            this.TotalPrice_label.Size = new System.Drawing.Size(57, 13);
            this.TotalPrice_label.TabIndex = 11;
            this.TotalPrice_label.Text = "Prix Total :";
            // 
            // Unit_Cost_label
            // 
            this.Unit_Cost_label.AutoSize = true;
            this.Unit_Cost_label.Location = new System.Drawing.Point(242, 42);
            this.Unit_Cost_label.Name = "Unit_Cost_label";
            this.Unit_Cost_label.Size = new System.Drawing.Size(86, 13);
            this.Unit_Cost_label.TabIndex = 10;
            this.Unit_Cost_label.Text = "Prix de l\'activité :";
            // 
            // AddMaterial_button
            // 
            this.AddMaterial_button.Location = new System.Drawing.Point(136, 66);
            this.AddMaterial_button.Name = "AddMaterial_button";
            this.AddMaterial_button.Size = new System.Drawing.Size(121, 23);
            this.AddMaterial_button.TabIndex = 9;
            this.AddMaterial_button.Text = "Ajouter Matériel";
            this.AddMaterial_button.UseVisualStyleBackColor = true;
            this.AddMaterial_button.Click += new System.EventHandler(this.AddMaterial_button_Click);
            // 
            // previous_button
            // 
            this.previous_button.Location = new System.Drawing.Point(319, 212);
            this.previous_button.Name = "previous_button";
            this.previous_button.Size = new System.Drawing.Size(75, 23);
            this.previous_button.TabIndex = 8;
            this.previous_button.Text = "Précédent";
            this.previous_button.UseVisualStyleBackColor = true;
            this.previous_button.Click += new System.EventHandler(this.previous_button_Click);
            // 
            // next_button2
            // 
            this.next_button2.Location = new System.Drawing.Point(400, 212);
            this.next_button2.Name = "next_button2";
            this.next_button2.Size = new System.Drawing.Size(75, 23);
            this.next_button2.TabIndex = 7;
            this.next_button2.Text = "Suivant";
            this.next_button2.UseVisualStyleBackColor = true;
            this.next_button2.Click += new System.EventHandler(this.next_button2_Click);
            // 
            // CheckMaterial_button
            // 
            this.CheckMaterial_button.Location = new System.Drawing.Point(9, 66);
            this.CheckMaterial_button.Name = "CheckMaterial_button";
            this.CheckMaterial_button.Size = new System.Drawing.Size(121, 23);
            this.CheckMaterial_button.TabIndex = 6;
            this.CheckMaterial_button.Text = "Vérifier le matériel";
            this.CheckMaterial_button.UseVisualStyleBackColor = true;
            this.CheckMaterial_button.Click += new System.EventHandler(this.CheckMaterial_button_Click);
            // 
            // equipmentDisponibility_comboBox
            // 
            this.equipmentDisponibility_comboBox.FormattingEnabled = true;
            this.equipmentDisponibility_comboBox.Location = new System.Drawing.Point(187, 39);
            this.equipmentDisponibility_comboBox.Name = "equipmentDisponibility_comboBox";
            this.equipmentDisponibility_comboBox.Size = new System.Drawing.Size(49, 21);
            this.equipmentDisponibility_comboBox.TabIndex = 5;
            this.equipmentDisponibility_comboBox.SelectedValueChanged += new System.EventHandler(this.equipmentDisponibility_comboBox_ValueChanged);
            this.equipmentDisponibility_comboBox.Click += new System.EventHandler(this.equipmentDisponibility_comboBox_Click);
            // 
            // SalaryFirstName_comboBox
            // 
            this.SalaryFirstName_comboBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.SalaryFirstName_comboBox.FormattingEnabled = true;
            this.SalaryFirstName_comboBox.Location = new System.Drawing.Point(187, 148);
            this.SalaryFirstName_comboBox.Name = "SalaryFirstName_comboBox";
            this.SalaryFirstName_comboBox.Size = new System.Drawing.Size(121, 21);
            this.SalaryFirstName_comboBox.TabIndex = 4;
            this.SalaryFirstName_comboBox.Text = "Prénom";
            this.SalaryFirstName_comboBox.Click += new System.EventHandler(this.SalaryFirstName_comboBox_Click);
            // 
            // SalaryLastName_comboBox
            // 
            this.SalaryLastName_comboBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.SalaryLastName_comboBox.FormattingEnabled = true;
            this.SalaryLastName_comboBox.Location = new System.Drawing.Point(60, 148);
            this.SalaryLastName_comboBox.Name = "SalaryLastName_comboBox";
            this.SalaryLastName_comboBox.Size = new System.Drawing.Size(121, 21);
            this.SalaryLastName_comboBox.TabIndex = 3;
            this.SalaryLastName_comboBox.Text = "Nom";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 151);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Moniteur";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Matériel";
            // 
            // Equipment_combobox
            // 
            this.Equipment_combobox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.Equipment_combobox.FormattingEnabled = true;
            this.Equipment_combobox.Location = new System.Drawing.Point(60, 39);
            this.Equipment_combobox.Name = "Equipment_combobox";
            this.Equipment_combobox.Size = new System.Drawing.Size(121, 21);
            this.Equipment_combobox.TabIndex = 0;
            this.Equipment_combobox.Text = "Type de matériel";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.print_button);
            this.tabPage3.Controls.Add(this.button7);
            this.tabPage3.Controls.Add(this.CommandReport_richTextBox);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(478, 238);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Résumé de la commande";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(400, 212);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 1;
            this.button7.Text = "Quitter";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // CommandReport_richTextBox
            // 
            this.CommandReport_richTextBox.Location = new System.Drawing.Point(0, 0);
            this.CommandReport_richTextBox.Name = "CommandReport_richTextBox";
            this.CommandReport_richTextBox.Size = new System.Drawing.Size(478, 206);
            this.CommandReport_richTextBox.TabIndex = 0;
            this.CommandReport_richTextBox.Text = "";
            // 
            // printDialog
            // 
            this.printDialog.UseEXDialog = true;
            // 
            // print_button
            // 
            this.print_button.Location = new System.Drawing.Point(319, 212);
            this.print_button.Name = "print_button";
            this.print_button.Size = new System.Drawing.Size(75, 23);
            this.print_button.TabIndex = 2;
            this.print_button.Text = "Imprimer";
            this.print_button.UseVisualStyleBackColor = true;
            this.print_button.Click += new System.EventHandler(this.print_button_Click);
            // 
            // printDocument1
            // 
            this.printDocument1.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.printDocument1_PrintPage);
            // 
            // form_Reservation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 262);
            this.Controls.Add(this.tabControl1);
            this.Name = "form_Reservation";
            this.Text = "Reservations";
            this.Load += new System.EventHandler(this.form_Reservation_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ComboBox CustomerFirstName_combobox;
        private System.Windows.Forms.ComboBox CustomerLastName_comboBox;
        private System.Windows.Forms.MaskedTextBox EndHour_maskedTextBox;
        private System.Windows.Forms.MaskedTextBox EndDay_maskedTextBox;
        private System.Windows.Forms.MaskedTextBox BeginHour_maskedTextBox;
        private System.Windows.Forms.MaskedTextBox BeginDay_maskedTextBox;
        private System.Windows.Forms.Label Client_label;
        private System.Windows.Forms.Label EndHour_label;
        private System.Windows.Forms.Label EndDay_label;
        private System.Windows.Forms.Label BeginHour_Label;
        private System.Windows.Forms.Label BeginDay_label;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.ComboBox equipmentDisponibility_comboBox;
        private System.Windows.Forms.ComboBox SalaryFirstName_comboBox;
        private System.Windows.Forms.ComboBox SalaryLastName_comboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox Equipment_combobox;
        private System.Windows.Forms.Button Next_button;
        private System.Windows.Forms.Button AddMaterial_button;
        private System.Windows.Forms.Button previous_button;
        private System.Windows.Forms.Button next_button2;
        private System.Windows.Forms.Button CheckMaterial_button;
        private System.Windows.Forms.Button DeleteMaterial_button;
        private System.Windows.Forms.Label TotalPrice_label;
        private System.Windows.Forms.Label Unit_Cost_label;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.RichTextBox CommandReport_richTextBox;
        private System.Windows.Forms.ComboBox CommandChoice_comboBox;
        private System.Windows.Forms.Label Unit_Cost_Modifiable_Label;
        private System.Windows.Forms.Label TotalPriceModifiable_label;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button AddMonitor_button;
        private System.Windows.Forms.Button button1_close;
        private System.Windows.Forms.Button print_button;
        private System.Windows.Forms.PrintDialog printDialog;
        private System.Drawing.Printing.PrintDocument printDocument1;
    }
}