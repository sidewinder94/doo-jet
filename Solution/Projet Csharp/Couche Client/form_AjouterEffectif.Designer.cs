﻿namespace Projet_Csharp.Couche_Client
{
    partial class form_AjouterEffectif
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1_add = new System.Windows.Forms.Button();
            this.textBox1_Secu = new System.Windows.Forms.TextBox();
            this.textBox5_PermisCotier = new System.Windows.Forms.TextBox();
            this.textBox6_BEES = new System.Windows.Forms.TextBox();
            this.textBox8_Prenom = new System.Windows.Forms.TextBox();
            this.textBox9_Nom = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox1_Statut = new System.Windows.Forms.ComboBox();
            this.button1_close = new System.Windows.Forms.Button();
            this.textBox2_DateEmb = new System.Windows.Forms.MaskedTextBox();
            this.textBox3_DateVMedic = new System.Windows.Forms.MaskedTextBox();
            this.textBox4_Contrat = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // button1_add
            // 
            this.button1_add.Location = new System.Drawing.Point(147, 249);
            this.button1_add.Name = "button1_add";
            this.button1_add.Size = new System.Drawing.Size(75, 23);
            this.button1_add.TabIndex = 0;
            this.button1_add.Text = "Enregistrer";
            this.button1_add.UseVisualStyleBackColor = true;
            this.button1_add.Click += new System.EventHandler(this.button1_add_Click);
            // 
            // textBox1_Secu
            // 
            this.textBox1_Secu.Location = new System.Drawing.Point(202, 12);
            this.textBox1_Secu.Name = "textBox1_Secu";
            this.textBox1_Secu.Size = new System.Drawing.Size(100, 20);
            this.textBox1_Secu.TabIndex = 1;
            // 
            // textBox5_PermisCotier
            // 
            this.textBox5_PermisCotier.Location = new System.Drawing.Point(202, 117);
            this.textBox5_PermisCotier.Name = "textBox5_PermisCotier";
            this.textBox5_PermisCotier.Size = new System.Drawing.Size(100, 20);
            this.textBox5_PermisCotier.TabIndex = 5;
            // 
            // textBox6_BEES
            // 
            this.textBox6_BEES.Location = new System.Drawing.Point(202, 143);
            this.textBox6_BEES.MaxLength = 10;
            this.textBox6_BEES.Name = "textBox6_BEES";
            this.textBox6_BEES.Size = new System.Drawing.Size(100, 20);
            this.textBox6_BEES.TabIndex = 6;
            this.textBox6_BEES.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            // 
            // textBox8_Prenom
            // 
            this.textBox8_Prenom.Location = new System.Drawing.Point(202, 196);
            this.textBox8_Prenom.Name = "textBox8_Prenom";
            this.textBox8_Prenom.Size = new System.Drawing.Size(100, 20);
            this.textBox8_Prenom.TabIndex = 8;
            // 
            // textBox9_Nom
            // 
            this.textBox9_Nom.Location = new System.Drawing.Point(202, 222);
            this.textBox9_Nom.Name = "textBox9_Nom";
            this.textBox9_Nom.Size = new System.Drawing.Size(100, 20);
            this.textBox9_Nom.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "N° Sécurité Sociale";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Date d\'embauche";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Date dernière Visite Medicale";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "Contrat de Travail";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Permis Côtier";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 146);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "BEES";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 172);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "Statut d\'Activité";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(13, 199);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Prénom";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 225);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Nom";
            // 
            // comboBox1_Statut
            // 
            this.comboBox1_Statut.FormattingEnabled = true;
            this.comboBox1_Statut.Items.AddRange(new object[] {
            "Plagiste"});
            this.comboBox1_Statut.Location = new System.Drawing.Point(202, 169);
            this.comboBox1_Statut.Name = "comboBox1_Statut";
            this.comboBox1_Statut.Size = new System.Drawing.Size(100, 21);
            this.comboBox1_Statut.TabIndex = 19;
            // 
            // button1_close
            // 
            this.button1_close.Location = new System.Drawing.Point(228, 249);
            this.button1_close.Name = "button1_close";
            this.button1_close.Size = new System.Drawing.Size(75, 23);
            this.button1_close.TabIndex = 20;
            this.button1_close.Text = "Retour";
            this.button1_close.UseVisualStyleBackColor = true;
            this.button1_close.Click += new System.EventHandler(this.button1_close_Click);
            // 
            // textBox2_DateEmb
            // 
            this.textBox2_DateEmb.Location = new System.Drawing.Point(203, 38);
            this.textBox2_DateEmb.Mask = "00/00/0000";
            this.textBox2_DateEmb.Name = "textBox2_DateEmb";
            this.textBox2_DateEmb.Size = new System.Drawing.Size(99, 20);
            this.textBox2_DateEmb.TabIndex = 21;
            this.textBox2_DateEmb.ValidatingType = typeof(System.DateTime);
            // 
            // textBox3_DateVMedic
            // 
            this.textBox3_DateVMedic.Location = new System.Drawing.Point(202, 64);
            this.textBox3_DateVMedic.Mask = "00/00/0000";
            this.textBox3_DateVMedic.Name = "textBox3_DateVMedic";
            this.textBox3_DateVMedic.Size = new System.Drawing.Size(100, 20);
            this.textBox3_DateVMedic.TabIndex = 22;
            // 
            // textBox4_Contrat
            // 
            this.textBox4_Contrat.FormattingEnabled = true;
            this.textBox4_Contrat.Location = new System.Drawing.Point(202, 90);
            this.textBox4_Contrat.Name = "textBox4_Contrat";
            this.textBox4_Contrat.Size = new System.Drawing.Size(100, 21);
            this.textBox4_Contrat.TabIndex = 23;
            // 
            // form_AjouterEffectif
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(314, 279);
            this.Controls.Add(this.textBox4_Contrat);
            this.Controls.Add(this.textBox3_DateVMedic);
            this.Controls.Add(this.textBox2_DateEmb);
            this.Controls.Add(this.button1_close);
            this.Controls.Add(this.comboBox1_Statut);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox9_Nom);
            this.Controls.Add(this.textBox8_Prenom);
            this.Controls.Add(this.textBox6_BEES);
            this.Controls.Add(this.textBox5_PermisCotier);
            this.Controls.Add(this.textBox1_Secu);
            this.Controls.Add(this.button1_add);
            this.Name = "form_AjouterEffectif";
            this.Text = "Ajouter un Effectif";
            this.Load += new System.EventHandler(this.form_AjouterEffectif_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1_add;
        private System.Windows.Forms.TextBox textBox1_Secu;
        private System.Windows.Forms.TextBox textBox5_PermisCotier;
        private System.Windows.Forms.TextBox textBox6_BEES;
        private System.Windows.Forms.TextBox textBox8_Prenom;
        private System.Windows.Forms.TextBox textBox9_Nom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox1_Statut;
        private System.Windows.Forms.Button button1_close;
        private System.Windows.Forms.MaskedTextBox textBox2_DateEmb;
        private System.Windows.Forms.MaskedTextBox textBox3_DateVMedic;
        private System.Windows.Forms.ComboBox textBox4_Contrat;
    }
}