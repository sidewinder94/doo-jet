﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Projet_Csharp.Couche_Client
{
    /// <summary>
    /// Affiche noms et prénoms de tous les effectifs
    /// </summary>
    public partial class form_AfficherToutEffectif : Form
    {
        /// <summary>
        /// création des objet data et Effectif
        /// </summary>
        private System.Data.DataSet oDATASET2;
        private Projet_Csharp.Couche_Métier.Pcs_Effectifs oEffectif;
        /// <summary>
        /// Constructeur par défaut, initialise les objets utilisés
        /// </summary>
        public form_AfficherToutEffectif()
        {
            this.oDATASET2 = new DataSet();
            this.oEffectif = new Projet_Csharp.Couche_Métier.Pcs_Effectifs();
            InitializeComponent();
        }

        /// <summary>
        /// affichage des données dans la GridView par l'appel de la fonction et de la requete SQL
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void form_AfficherTout_Load(object sender, EventArgs e)
        {
            this.oDATASET2 = this.oEffectif.m_afficherEffectifs("rwsEffectif");
            this.dataGridView1.DataSource = this.oDATASET2.Tables["rwsEffectif"];
        }

        /// <summary>
        /// fermeture du formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_close_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
