﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Projet_Csharp.Couche_Client
{
    /// <summary>
    /// Formulaire de suppression d'une activité
    /// </summary>
    public partial class form_SupressionActivité : Form
    {
        /// <summary>
        /// création de l'objet
        /// </summary>
        private Couche_Métier.Pcs_Reservations oReservation;
        private int commandID;

        /// <summary>
        /// Fenetre de gestion d'erreur
        /// </summary>
        public form_SupressionActivité()
        {
            InitializeComponent();
            MessageBox.Show("Veuillez sélectionner une commande dans la fenêtre"
                + " de gestion des réservations", "Attention", MessageBoxButtons.OK,
                MessageBoxIcon.Warning);
            this.Close();
        }

        /// <summary>
        /// Constructeur paramétré permettant d'initialiser les objets utilisés
        /// et de récupérer l'ID de la commande a traiter
        /// </summary>
        /// <param name="commandID"></param>
        public form_SupressionActivité(int commandID)
        {
            InitializeComponent();
            this.commandID = commandID;
            oReservation = new Couche_Métier.Pcs_Reservations();
            actualisationDonnées();
        }

        /// <summary>
        /// fermeture du formulaire
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitForm_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// appel de la fonction puis requete SQL permettant l'action demandée
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void delete_button_Click(object sender, EventArgs e)
        {
            oReservation.m_deleteEquipmentReservation(commandID, Convert.ToString(((int)equipmentdeleteselction_combobox.SelectedValue)));
            actualisationDonnées();
        }
        /// <summary>
        /// Actualisation des données
        /// </summary>
        private void actualisationDonnées()
        {
            DataSet temp = oReservation.m_GetEquipmentReservations(commandID, "equipresa");
            equipmentdeleteselction_combobox.DataSource = temp.Tables["equipresa"];
            equipmentdeleteselction_combobox.ValueMember = "N° Equipement";
            equipmentdeleteselction_combobox.DisplayMember = "Nom";
            dataGridView1.DataSource = temp.Tables["equipresa"];
            dataGridView1.Columns.Remove("N° Equipement");
        }
    }
}
