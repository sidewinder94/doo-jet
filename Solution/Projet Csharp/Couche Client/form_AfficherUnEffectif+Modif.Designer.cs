﻿namespace Projet_Csharp.Couche_Client
{
    partial class form_AfficherUnEffectif_Modif
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1_Statut = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.textBox6_BEES = new System.Windows.Forms.TextBox();
            this.textBox5_PermisCotier = new System.Windows.Forms.TextBox();
            this.textBox1_Secu = new System.Windows.Forms.TextBox();
            this.button1_MiseàJ = new System.Windows.Forms.Button();
            this.button3_suppr = new System.Windows.Forms.Button();
            this.button1_close = new System.Windows.Forms.Button();
            this.textBox1_Nom = new System.Windows.Forms.ComboBox();
            this.FirstName_comboBox = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.textBox8_Prenom = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox9_nom = new System.Windows.Forms.TextBox();
            this.textBox2_DateEmb = new System.Windows.Forms.MaskedTextBox();
            this.textBox3_DateVMedic = new System.Windows.Forms.MaskedTextBox();
            this.textBox4_Contrat = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nom";
            // 
            // comboBox1_Statut
            // 
            this.comboBox1_Statut.FormattingEnabled = true;
            this.comboBox1_Statut.Items.AddRange(new object[] {
            "Plagiste"});
            this.comboBox1_Statut.Location = new System.Drawing.Point(443, 59);
            this.comboBox1_Statut.Name = "comboBox1_Statut";
            this.comboBox1_Statut.Size = new System.Drawing.Size(100, 21);
            this.comboBox1_Statut.TabIndex = 37;
            this.comboBox1_Statut.Click += new System.EventHandler(this.comboBox1_Statut_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(356, 88);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(43, 13);
            this.label8.TabIndex = 35;
            this.label8.Text = "Prénom";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(356, 62);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 13);
            this.label7.TabIndex = 34;
            this.label7.Text = "Statut d\'Activité";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(356, 36);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "BEES";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 141);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 32;
            this.label5.Text = "Permis Côtier";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "Contrat de Travail";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(44, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(145, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "Date dernière Visite Medicale";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "Date d\'embauche";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(44, 36);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(99, 13);
            this.label10.TabIndex = 28;
            this.label10.Text = "N° Sécurité Sociale";
            // 
            // textBox6_BEES
            // 
            this.textBox6_BEES.Location = new System.Drawing.Point(443, 33);
            this.textBox6_BEES.MaxLength = 10;
            this.textBox6_BEES.Name = "textBox6_BEES";
            this.textBox6_BEES.Size = new System.Drawing.Size(100, 20);
            this.textBox6_BEES.TabIndex = 25;
            // 
            // textBox5_PermisCotier
            // 
            this.textBox5_PermisCotier.Location = new System.Drawing.Point(195, 138);
            this.textBox5_PermisCotier.Name = "textBox5_PermisCotier";
            this.textBox5_PermisCotier.Size = new System.Drawing.Size(100, 20);
            this.textBox5_PermisCotier.TabIndex = 24;
            // 
            // textBox1_Secu
            // 
            this.textBox1_Secu.Location = new System.Drawing.Point(195, 33);
            this.textBox1_Secu.Name = "textBox1_Secu";
            this.textBox1_Secu.Size = new System.Drawing.Size(100, 20);
            this.textBox1_Secu.TabIndex = 20;
            // 
            // button1_MiseàJ
            // 
            this.button1_MiseàJ.Location = new System.Drawing.Point(468, 141);
            this.button1_MiseàJ.Name = "button1_MiseàJ";
            this.button1_MiseàJ.Size = new System.Drawing.Size(75, 28);
            this.button1_MiseàJ.TabIndex = 40;
            this.button1_MiseàJ.Text = "Mettre à jour";
            this.button1_MiseàJ.UseVisualStyleBackColor = true;
            this.button1_MiseàJ.Click += new System.EventHandler(this.button1_MiseàJ_Click);
            // 
            // button3_suppr
            // 
            this.button3_suppr.Location = new System.Drawing.Point(359, 4);
            this.button3_suppr.Name = "button3_suppr";
            this.button3_suppr.Size = new System.Drawing.Size(75, 23);
            this.button3_suppr.TabIndex = 42;
            this.button3_suppr.Text = "Supprimer";
            this.button3_suppr.UseVisualStyleBackColor = true;
            this.button3_suppr.Click += new System.EventHandler(this.button3_suppr_Click);
            // 
            // button1_close
            // 
            this.button1_close.Location = new System.Drawing.Point(387, 141);
            this.button1_close.Name = "button1_close";
            this.button1_close.Size = new System.Drawing.Size(75, 28);
            this.button1_close.TabIndex = 44;
            this.button1_close.Text = "Retour";
            this.button1_close.UseVisualStyleBackColor = true;
            this.button1_close.Click += new System.EventHandler(this.button1_close_Click);
            // 
            // textBox1_Nom
            // 
            this.textBox1_Nom.FormattingEnabled = true;
            this.textBox1_Nom.Location = new System.Drawing.Point(47, 6);
            this.textBox1_Nom.Name = "textBox1_Nom";
            this.textBox1_Nom.Size = new System.Drawing.Size(121, 21);
            this.textBox1_Nom.TabIndex = 45;
            // 
            // FirstName_comboBox
            // 
            this.FirstName_comboBox.FormattingEnabled = true;
            this.FirstName_comboBox.Location = new System.Drawing.Point(232, 6);
            this.FirstName_comboBox.Name = "FirstName_comboBox";
            this.FirstName_comboBox.Size = new System.Drawing.Size(121, 21);
            this.FirstName_comboBox.TabIndex = 46;
            this.FirstName_comboBox.SelectionChangeCommitted += new System.EventHandler(this.button2_trouver_Click);
            this.FirstName_comboBox.Click += new System.EventHandler(this.FirstName_Combobox_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(174, 9);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 47;
            this.label9.Text = "Prénom : ";
            // 
            // textBox8_Prenom
            // 
            this.textBox8_Prenom.Location = new System.Drawing.Point(443, 85);
            this.textBox8_Prenom.Name = "textBox8_Prenom";
            this.textBox8_Prenom.Size = new System.Drawing.Size(100, 20);
            this.textBox8_Prenom.TabIndex = 26;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(356, 114);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(29, 13);
            this.label11.TabIndex = 48;
            this.label11.Text = "Nom";
            // 
            // textBox9_nom
            // 
            this.textBox9_nom.Location = new System.Drawing.Point(443, 111);
            this.textBox9_nom.Name = "textBox9_nom";
            this.textBox9_nom.Size = new System.Drawing.Size(100, 20);
            this.textBox9_nom.TabIndex = 49;
            // 
            // textBox2_DateEmb
            // 
            this.textBox2_DateEmb.Location = new System.Drawing.Point(195, 59);
            this.textBox2_DateEmb.Mask = "00/00/0000";
            this.textBox2_DateEmb.Name = "textBox2_DateEmb";
            this.textBox2_DateEmb.Size = new System.Drawing.Size(100, 20);
            this.textBox2_DateEmb.TabIndex = 50;
            this.textBox2_DateEmb.ValidatingType = typeof(System.DateTime);
            // 
            // textBox3_DateVMedic
            // 
            this.textBox3_DateVMedic.Location = new System.Drawing.Point(195, 85);
            this.textBox3_DateVMedic.Mask = "00/00/0000";
            this.textBox3_DateVMedic.Name = "textBox3_DateVMedic";
            this.textBox3_DateVMedic.Size = new System.Drawing.Size(100, 20);
            this.textBox3_DateVMedic.TabIndex = 51;
            this.textBox3_DateVMedic.ValidatingType = typeof(System.DateTime);
            // 
            // textBox4_Contrat
            // 
            this.textBox4_Contrat.FormattingEnabled = true;
            this.textBox4_Contrat.Location = new System.Drawing.Point(195, 111);
            this.textBox4_Contrat.Name = "textBox4_Contrat";
            this.textBox4_Contrat.Size = new System.Drawing.Size(100, 21);
            this.textBox4_Contrat.TabIndex = 52;
            // 
            // form_AfficherUnEffectif_Modif
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(556, 179);
            this.Controls.Add(this.textBox4_Contrat);
            this.Controls.Add(this.textBox3_DateVMedic);
            this.Controls.Add(this.textBox2_DateEmb);
            this.Controls.Add(this.textBox9_nom);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.FirstName_comboBox);
            this.Controls.Add(this.textBox1_Nom);
            this.Controls.Add(this.button1_close);
            this.Controls.Add(this.button3_suppr);
            this.Controls.Add(this.button1_MiseàJ);
            this.Controls.Add(this.comboBox1_Statut);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBox8_Prenom);
            this.Controls.Add(this.textBox6_BEES);
            this.Controls.Add(this.textBox5_PermisCotier);
            this.Controls.Add(this.textBox1_Secu);
            this.Controls.Add(this.label1);
            this.Name = "form_AfficherUnEffectif_Modif";
            this.Text = "Affichage détaillée et modification d\'un Effectif";
            this.Load += new System.EventHandler(this.form_AfficherUnEffectif_Modif_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1_Statut;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox6_BEES;
        private System.Windows.Forms.TextBox textBox5_PermisCotier;
        private System.Windows.Forms.TextBox textBox1_Secu;
        private System.Windows.Forms.Button button1_MiseàJ;
        private System.Windows.Forms.Button button3_suppr;
        private System.Windows.Forms.Button button1_close;
        private System.Windows.Forms.ComboBox textBox1_Nom;
        private System.Windows.Forms.ComboBox FirstName_comboBox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBox8_Prenom;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox9_nom;
        private System.Windows.Forms.MaskedTextBox textBox2_DateEmb;
        private System.Windows.Forms.MaskedTextBox textBox3_DateVMedic;
        private System.Windows.Forms.ComboBox textBox4_Contrat;

        public System.EventHandler textBox1_Nom_TextChanged { get; set; }
    }
}