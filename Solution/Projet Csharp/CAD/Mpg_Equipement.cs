﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Projet_Csharp.CAD
{
    /// <summary>
    /// Classe de mappage regroupant les requêtes SQL correspondant à l'equipement
    /// </summary>
    class Mpg_Equipement
    {
        /// <summary>
        /// Sélectionne tout les nom et numéro de série de l'équipement
        /// </summary>
        /// <returns></returns>
        public string m_getAllEquimpent()
        {
            return "SELECT Equipement.Nom, Equipement.[N° Série] FROM Equipement";
        }
        /// <summary>
        /// Sélectionne la disponibilité d'un équipement suivant son nom
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string m_GetDisponibilityByName(string name)
        {
            return "SELECT Disponibilité FROM Equipement WHERE [Nom] = '" + name + "';";
        }
        /// <summary>
        /// Sélectionne le cout de l'équipement suivant son nom 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string m_GetPrice(string name)
        {
            return "SELECT [Cout HT Location] From Equipement WHERE [Nom] = '" + name + "';";
        }
        /// <summary>
        /// Met à jours le quantité d'équipement pour la disponibilité d'après l'id 
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="quantity"></param>
        /// <returns></returns>
        public string m_SetDisponibility(int ID, int quantity)
        {
            return "UPDATE Equipement SET Equipement.Disponibilité = " + quantity + " WHERE (([Equipement].[N° Identification]=" + ID + "));";
        }
        /// <summary>
        /// Sélectionne le numéro d'identification d'près le nom de l'équipement de l'équipement
        /// </summary>
        /// <param name="equipmentName"></param>
        /// <returns></returns>
        public string m_GetIDByName(string equipmentName)
        {
            return "SELECT [N° Identification] FROM Equipement WHERE [Nom] = '" + equipmentName + "';";
        }
        /// <summary>
        /// Sélectionne tout d'un équipement d'après son nom
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public string m_GetOneEquipement(string name)
        {
            return "SELECT * FROM Equipement WHERE [Nom] = '" + name + "';";
        }
        /// <summary>
        /// Requete SQL pour création Equipement
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="descriptif"></param>
        /// <param name="puissance"></param>
        /// <param name="disponibilité"></param>
        /// <param name="CoutHT"></param>
        /// <param name="TypeAct"></param>
        /// <returns></returns>
        public string m_InsertEquipement(string nom, string descriptif, string puissance, int disponibilité, int CoutHT, int TypeAct, int nbtotal)
        {
            return "INSERT INTO Equipement ([Nom], [Descriptif], [Puissance], [Disponibilité], [Cout HT Location], [Type Activité] ,[Quantité])"
                    + "VALUES ('" + nom + "','" + descriptif + "','" + puissance + "','" + disponibilité + "','" + CoutHT + "','" + TypeAct + "','"+nbtotal+"');";
        }
        /// <summary>
        /// Requete SQL pour Supprimer un Equipement
        /// </summary>
        /// <param name="nom"></param>
        /// <returns></returns>
        public string m_DeleteEquipement(string nom)
        {
            return "DELETE * FROM Equipement WHERE [Nom] = '" + nom + "';";
        }
        /// <summary>
        /// Requete SQL pour mettre un Equipement à jour
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="descriptif"></param>
        /// <param name="puissance"></param>
        /// <param name="disponibilité"></param>
        /// <param name="CoutHT"></param>
        /// <param name="TypeAct"></param>
        /// <param name="nb_total"</param>
        /// <returns></returns>
        public string m_UpdateEquipement(string nom, string descriptif, string puissance, int disponibilité, int CoutHT, int TypeAct, int nb_total)
        {
            return "UPDATE Equipement SET Equipement.[Descriptif] = '" + descriptif + "', Equipement.[Puissance] = '" + puissance + "', Equipement.[Disponibilité] = '" + disponibilité + "', Equipement.[Cout HT Location] = '" + CoutHT + "', Equipement.[Type Activité] = '" + TypeAct + "', Equipement.[Quantité] = " + nb_total + " WHERE Equipement.[Nom] = '" + nom + "';";
        }

        /// <summary>
        /// Récupère toutes les infos de tous les équipements
        /// </summary>
        /// <returns></returns>
        public string m_getAllEquimpentData()
        {
            return "SELECT * FROM Equipement;";
        }
    }
}
