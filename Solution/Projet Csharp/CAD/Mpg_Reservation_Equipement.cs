﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Projet_Csharp.CAD
{
    /// <summary>
    /// Classe de mappage regroupant toutes les requêtes SQL concernant la reservation
    /// et les equipements
    /// </summary>
    class Mpg_Reservation_Equipement
    {
        
        /// <summary>
        /// Stub de méthode permettant d'obtenir le cout total d'une commande
        /// </summary>
        /// <param name="commandreference"></param>
        /// <returns></returns>
        public double m_GetCommandCost(int commandreference)
        {
            throw new NotImplementedException();
        }
        /// <summary>
        /// Insert dans réservation_équipement le prix et le nombre de l'équipement en question qui est réserver
        /// </summary>
        /// <param name="commande_ID"></param>
        /// <param name="equipmentID"></param>
        /// <param name="number"></param>
        /// <param name="cost"></param>
        /// <returns></returns>
        public string m_AddByName(int commande_ID, int equipmentID, int number,double cost)
        {
            return "INSERT INTO Reservation_Equipement ( [N° Reservation], [N° Equipement], Coût, Nombre ) VALUES ('" + commande_ID + "','" + equipmentID + "', '" + cost + "','" + number + "');";
 
        }
        /// <summary>
        /// Sélectionne le numéro de l'équipement dans la table réservation_équipement d'après le numéro de réservation
        /// </summary>
        /// <param name="commandID"></param>
        /// <returns></returns>
        public string mGetProductByCommandID(int commandID)
        {
            return "SELECT [N° Equipement] FROM Reservation_Equipement WHERE [N° Reservation] = " + commandID + ";";
        }
        /// <summary>
        /// Retourne une requete permettant d'obtenir la liste, la quantité et le prix des produits demandés
        /// </summary>
        /// <param name="commandID"></param>
        /// <returns></returns>
        public string m_GetReservationEquipment(string commandID)
        {
            return "SELECT Equipement.Nom, Reservation_Equipement.Nombre, Reservation_Equipement.Coût "
            + "FROM Reservation INNER JOIN (Equipement INNER JOIN Reservation_Equipement "
            + "ON Equipement.[N° Identification] = Reservation_Equipement.[N° Equipement]) "
            + "ON Reservation.[N° Reservation] = Reservation_Equipement.[N° Reservation] "
            + "GROUP BY Reservation.[N° Reservation], Equipement.Nom, Reservation_Equipement.Nombre, "
            + "Reservation_Equipement.Coût HAVING (((Reservation.[N° Reservation])=" + commandID + "));";

        }
        /// <summary>
        /// Retourne une requête permettant d'obtenir les equipements reservés pour une
        /// reservation
        /// </summary>
        /// <param name="commandID"></param>
        /// <returns></returns>
        public string mGetAllProductByCommandID(int commandID)
        {
            return "SELECT Equipement.Nom, Reservation_Equipement.Coût, "
                +"Reservation_Equipement.Nombre, Reservation_Equipement.[N° Equipement] "
                + "FROM Equipement INNER JOIN Reservation_Equipement "
                + "ON Equipement.[N° Identification] = Reservation_Equipement.[N° Equipement] "
                + "WHERE (((Reservation_Equipement.[N° Reservation])="+commandID+"));";

        }
        /// <summary>
        /// Supprime la réservation et l'équipement associé choisi d'après le numéro de la réservation et le numéro de l'équipement
        /// </summary>
        /// <param name="commandID"></param>
        /// <param name="equipmentID"></param>
        /// <returns></returns>
        public string m_DeleteAssociation(int commandID, string equipmentID)
        {
            return "DELETE Reservation_Equipement.*, Reservation_Equipement.[N° Reservation], Reservation_Equipement.[N° Equipement] "
            +"FROM Reservation_Equipement "
            +"WHERE (((Reservation_Equipement.[N° Reservation])="+commandID+") "
            +"AND ((Reservation_Equipement.[N° Equipement])="+equipmentID+"));";
        }
    }
}
