﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Projet_Csharp.CAD
{
    /// <summary>
    /// Classe de mappage regroupant les requetes SQL concernant et les effectifs
    /// et la reservation
    /// </summary>
    class Mpg_Reservation_Effectif
    {

        /// <summary>
        /// Ajoute une assoiation effectif <-> reservation
        /// </summary>
        /// <param name="salaryID"></param>
        /// <param name="commmandID"></param>
        /// <returns></returns>
        public string AddAssociation(int salaryID, string commmandID)
        {
            return "INSERT INTO Reservation_Effectif ([N° Effectif],[N° Reservation])"
                +" VALUES ('"+salaryID+"','"+commmandID+"');";
        }
        /// <summary>
        /// Permet d'afficher le prix total de d'un réservation avec ou sans un moniteur 
        /// </summary>
        /// <param name="commandID"></param>
        /// <returns></returns>
        public string m_CountByID(string commandID)
        {
            return "SELECT Count(*) AS Total "
            +"FROM (SELECT Reservation_Effectif.[N° Reservation], Effectif.Nom, Effectif.Prénom "
            +"FROM Effectif INNER JOIN Reservation_Effectif "
            +"ON Effectif.[N° Effectif] = Reservation_Effectif.[N° Effectif] "
            +"WHERE (((Reservation_Effectif.[N° Reservation])="+commandID+")));";
        }
        /// <summary>
        /// S$Permet de sélectionner le moniteur de la réservation
        /// </summary>
        /// <param name="commandID"></param>
        /// <returns></returns>
       public string m_GetReservationEffectif(string commandID)
        {
            return "SELECT Effectif.Nom, Effectif.Prénom, Effectif.[Statut Activité] "
                + "FROM Effectif INNER JOIN Reservation_Effectif "
                +"ON Effectif.[N° Effectif] = Reservation_Effectif.[N° Effectif] "
                + "WHERE (((Reservation_Effectif.[N° Reservation])="+commandID+"));";

        }
    }
}
