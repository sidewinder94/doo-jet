﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Projet_Csharp.CAD
{
    /// <summary>
    /// Mappage permettant l'accès aux activités
    /// </summary>
    class Mpg_Activité
    {
        /// <summary>
        /// Récupère toutes les infos de toutes les activités
        /// </summary>
        /// <returns></returns>
        public string m_getAll()
        {
            return "SELECT * From Activité;";
        }
    }
}
