﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Projet_Csharp.CAD
{
    /// <summary>
    /// Classe de mappage regroupant toutes les requetes SQL concernant les effectifs
    /// </summary>
    class Mpg_Effectif
    {
        /// <summary>
        /// Sélectionne le nom et le prénom de l'effectif
        /// </summary>
        /// <param name="begin_date"></param>
        /// <param name="begin_hour"></param>
        /// <param name="end_date"></param>
        /// <param name="end_hour"></param>
        /// <returns></returns>
        public string m_GetNameByDisponibility(string begin_date, string begin_hour, string end_date, string end_hour)
        {
            /*Fonction de Stub*/
            return "SELECT Nom,Prénom FROM Effectif;";
        }
        /// <summary>
        /// Sélectionne le nom et le prénom de la table effectif d'après le nom
        /// </summary>
        /// <param name="begin_date"></param>
        /// <param name="begin_hour"></param>
        /// <param name="end_date"></param>
        /// <param name="end_hour"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public string m_GetFirstNameByDisponibility(string begin_date, string begin_hour, string end_date, string end_hour,string name)
        {
            /*Fonction de Stub*/
            return "SELECT Nom,Prénom FROM Effectif WHERE [Nom] = '" + name + "';";
        }
        /// <summary>
        /// Requete SQL pour ajouter un effectif
        /// </summary>
        /// <param name="Secu"></param>
        /// <param name="Embauche"></param>
        /// <param name="VisiteMedical"></param>
        /// <param name="contratTravail"></param>
        /// <param name="PermisCotier"></param>
        /// <param name="BEES"></param>
        /// <param name="StatutActivite"></param>
        /// <param name="Prenom"></param>
        /// <param name="Nom"></param>
        /// <returns></returns>
        public string AjouterEffectif(string Secu, string Embauche, string VisiteMedical, int contratTravail, string PermisCotier, string BEES, string StatutActivite, string Prenom, string Nom)
        {
            return "INSERT INTO Effectif ([N° Securité sociale], [Date d'embauche], [Date Derniere Visite], [Statut Contrat], [N° Permis Cotier], [N° BEES], [Statut Activité], [Prénom], [Nom])"
                           + "VALUES ('" + Secu + "','" + Embauche + "','" + VisiteMedical + "','" + contratTravail + "','" + PermisCotier + "','" + BEES + "','" + StatutActivite + "','" + Prenom + "','" + Nom + "');";
        }
        /// <summary>
        /// Requete SQL pour afficher tous les effectifs
        /// </summary>
        /// <returns></returns>
        public string AfficherEffectif()
        {
            return "SELECT Nom, Prénom FROM Effectif ;";
        }
        /// <summary>
        /// Requete SQL pour afficher un Effectif en particulier
        /// le prénom est optionnel même si grandement recommandé pour obtenir une seule réponse
        /// </summary>
        /// <param name="nom"></param>
        /// <returns></returns>
        public string AfficherParNomPrenom(string nom, string prenom = null)
        {
            if (prenom == null)
            {
                return "SELECT * FROM Effectif WHERE [Nom] = '" + nom + "';";
            }
            else
            {
                return "SELECT * FROM Effectif WHERE [Nom] = '" + nom + "' AND [Prénom] = '" + prenom + "';";
            }
        }
        /// <summary>
        /// Requete SQL pour effacer un effectif
        /// </summary>
        /// <param name="nom"></param>
        /// <returns></returns>
        public string EffacerEffectif(string nom)
        {
            return "DELETE * FROM Effectif WHERE [Nom] = '" + nom + "';";
        }

        /// <summary>
        /// Requete SQL de Mise à Jour d'un effectif
        /// </summary>
        /// <param name="Secu"></param>
        /// <param name="Embauche"></param>
        /// <param name="VisiteMedical"></param>
        /// <param name="contratTravail"></param>
        /// <param name="PermisCotier"></param>
        /// <param name="BEES"></param>
        /// <param name="StatutActivite"></param>
        /// <param name="Prenom"></param>
        /// <param name="Nom"></param>
        /// <returns></returns>
        /// 
        public string MettreAjour(string Secu, string Embauche, string VisiteMedical, int contratTravail, string PermisCotier, string BEES, string StatutActivite, string Prenom, string Nom,int ID)
        {
            return "UPDATE Effectif SET Effectif.[N° Securité sociale] = '" + Secu + "', Effectif.[Date d'embauche] = '" + Embauche + "', Effectif.[Date Derniere Visite] = '" + VisiteMedical + "', Effectif.[Statut Contrat] = " + contratTravail + ", Effectif.[N° Permis Cotier] = '" + PermisCotier + "', Effectif.[N° BEES] = '" + BEES + "', Effectif.[Statut Activité] = '" + StatutActivite + "', Effectif.Prénom = '" + Prenom + "', Effectif.Nom =  '" + Nom + "' WHERE [N° Effectif] = "+ID+";";
        }

        /// <summary>
        /// Permet d'obtenir l'ID d'un salarié précis
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        public string m_GetSalaryID(string firstName, string lastName)
        {
            return "SELECT [N° Effectif] FROM Effectif WHERE [Nom] = '" + lastName + "' AND [Prénom] = '" + firstName + "';";
        }
    }
}
