﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Projet_Csharp.CAD
{
    /// <summary>
    /// Class de mappage regroupant les requetes SQL concernant la réservation
    /// </summary>
    class Mpg_reservation
    {
        /// <summary>
        /// Insert dans la réservation les dates et les heures
        /// </summary>
        /// <param name="BeginDay"></param>
        /// <param name="BeginHour"></param>
        /// <param name="EndDay"></param>
        /// <param name="EndHour"></param>
        /// <param name="cost"></param>
        /// <param name="customer_ID"></param>
        /// <returns></returns>
        public string m_AddReservation(string BeginDay, string BeginHour, string EndDay,
            string EndHour, double cost, int customer_ID)
        {
            return "INSERT INTO Reservation ([DateDebut],[HeureDebut],[DateFin],"
            + "[HeureFin],[Coût],[Client]) VALUES"
            + "('" + BeginDay + "','" + BeginHour + "','" + EndDay + "','" + EndHour + "','" + cost + "','" + customer_ID + "');";
        }
        /// <summary>
        /// Sélectionne le dernier numéro de réservation de la table réservation 
        /// </summary>
        /// <returns></returns>
        public string m_GetLast()
        {
            return "SELECT Max(Reservation.[N° Reservation]) AS N°Reservation FROM Reservation;";
        }
        /// <summary>
        /// Sélectionne le numéro de réservation de la table réservation
        /// </summary>
        /// <returns></returns>
        public string m_GetAll()
        {
            return "SELECT [N° Reservation] As Reservation FROM Reservation;";
        }
        /// <summary>
        /// Sélectionne tout de la table réservation d'après le numéro de réservation
        /// </summary>
        /// <param name="commandID"></param>
        /// <returns></returns>
        public string m_GetByID(int commandID)
        {
            return "SELECT * FROM Reservation WHERE [N° Reservation] = " + commandID + ";";
        }
        /// <summary>
        /// Sélectionne cout de la table réservation d'après le numéro de réservation
        /// </summary>
        /// <param name="commandID"></param>
        /// <returns></returns>
        public string m_GetPriceByID(int commandID)
        {
            return "SELECT [Coût] FROM Reservation WHERE [N° Reservation] = " + commandID + ";";
        }
        /// <summary>
        /// Mise à jours du cout et du numéro de la réservation
        /// </summary>
        /// <param name="cost"></param>
        /// <param name="command_ID"></param>
        /// <returns></returns>
        public string m_SetCostByID(double cost, int command_ID)
        {
            return "UPDATE Reservation SET [Reservation].[Coût] = " + cost + " WHERE (([Reservation].[N° Reservation] = " + command_ID + "));";
        }
        /// <summary>
        /// Sélectionne le client de la réservation d'après le numéro de la réservation
        /// </summary>
        /// <param name="commandID"></param>
        /// <returns></returns>
        public string GetCustomerID(string commandID)
        {
            return "SELECT [Client] FROM Reservation WHERE [N° Reservation] = " + commandID + ";";
        }
        /// <summary>
        /// Retourne un requete obtenant le nombre de matériels commandés
        /// dans une commande
        /// </summary>
        /// <param name="commandID"></param>
        /// <returns></returns>
        public string m_CountByID(string commandID)
        {
            return "SELECT Count(*) AS Total FROM ("
            +"SELECT Equipement.Nom, Reservation_Equipement.Nombre, Reservation_Equipement.Coût "
            +"FROM Reservation INNER JOIN (Equipement INNER JOIN Reservation_Equipement "
            +"ON Equipement.[N° Identification] = Reservation_Equipement.[N° Equipement]) "
            +"ON Reservation.[N° Reservation] = Reservation_Equipement.[N° Reservation] "
            +"GROUP BY Reservation.[N° Reservation], Equipement.Nom, Reservation_Equipement.Nombre, "
            +"Reservation_Equipement.Coût HAVING (((Reservation.[N° Reservation])="+commandID+")));";
        }
        /// <summary>
        /// Récupère toutes les informationsconcernant toutes les réservations
        /// </summary>
        /// <returns></returns>
        public string m_GetAllData()
        {
            return "SELECT * FROM Reservation";
        }
    }
}
