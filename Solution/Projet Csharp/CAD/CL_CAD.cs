﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.OleDb;

namespace Projet_Csharp.CAD
{
    /// <summary>
    /// Class permettant l'accès aux données
    /// </summary>
    class CL_CAD
    {
        /// <summary>
        /// Appel des données qui sont utilisé dans le CAD
        /// </summary>
        private string rq_sql;
        private string cnx;
        private System.Data.OleDb.OleDbConnection oCNX;
        private System.Data.OleDb.OleDbCommand oCMD;
        private System.Data.OleDb.OleDbDataAdapter oDA;
        private System.Data.DataSet oDS;

        /// <summary>
        /// Initialise les variables ainsi que la connexion à la base de données
        /// </summary>
        public CL_CAD()
        {
            this.rq_sql = "NC";
#if DEBUG
            this.cnx = Properties.Settings.Default.ConnexionStringDebug;
#else
             this.cnx = Properties.Settings.Default.ConnexionString;
#endif
            this.oCNX = new OleDbConnection(this.cnx);
            this.oCMD = new OleDbCommand();
            this.oDA = new OleDbDataAdapter();
        }

        /// <summary>
        /// Récupération des données se situant dans le mappage et renvoie d'un tableau de la base de données
        /// </summary>
        /// <param name="rq_sql"></param>
        /// <param name="rowsName"></param>
        /// <returns></returns>
        public System.Data.DataSet m_GetRows(string rq_sql, string rowsName)
        {
            this.oDS = new System.Data.DataSet();
            this.rq_sql = rq_sql;
            this.oCMD.Connection = this.oCNX;
            this.oCMD.CommandText = this.rq_sql;
            this.oDA.SelectCommand = this.oCMD;
            this.oDA.Fill(this.oDS, rowsName);
            return this.oDS;
        }

        /// <summary>
        /// Récupération des données se situant dans le mappage et insert des données dans la base de données
        /// </summary>
        /// <param name="rq_sql"></param>
        public void m_ActionRows(string rq_sql)
        {
            try { oCNX.Close(); }
            catch { }
            this.rq_sql = rq_sql;
            this.oCMD.Connection = this.oCNX;
            this.oCMD.CommandText = this.rq_sql;
            this.oCNX.Open();
            this.oCMD.ExecuteNonQuery();
            this.oCNX.Close();
        }


        /// <summary>
        /// Permet d'exécuter une requête ne retournant qu'une seule valeur
        /// Soit un entier, soit un décimal, soit des caractères
        /// </summary>
        /// <param name="rq_sql"></param>
        /// <returns></returns>
        public int m_Scalar_int(string rq_sql)
        {
            try { oCNX.Close(); }
            catch { }
            int temp;
            this.rq_sql = rq_sql;
            this.oCMD.Connection = this.oCNX;
            this.oCMD.CommandText = this.rq_sql;
            this.oCNX.Open();
            temp = (int)this.oCMD.ExecuteScalar();
            this.oCNX.Close();
            return temp;
        }
        /// <summary>
        /// Permet d'exécuter une requete retournant une seule valeur
        /// Retourne un double
        /// </summary>
        /// <param name="rq_sql"></param>
        /// <returns></returns>
        public double m_Scalar_double(string rq_sql)
        {
            try { oCNX.Close(); }
            catch { }
            double temp;
            this.rq_sql = rq_sql;
            this.oCMD.Connection = this.oCNX;
            this.oCMD.CommandText = this.rq_sql;
            this.oCNX.Open();
            temp = (double)this.oCMD.ExecuteScalar();
            this.oCNX.Close();
            return temp;
        }
        /// <summary>
        /// Permet d'exécuter une requete retournant une seule valeur
        /// Retourne un string
        /// </summary>
        /// <param name="rq_sql"></param>
        /// <returns></returns>
        public string m_Scalar_string(string rq_sql)
        {
            try { oCNX.Close(); }
            catch { }
            string temp;
            this.rq_sql = rq_sql;
            this.oCMD.Connection = this.oCNX;
            this.oCMD.CommandText = this.rq_sql;
            this.oCNX.Open();
            temp = (string)this.oCMD.ExecuteScalar();
            this.oCNX.Close();
            return temp;
        }

    }
}
