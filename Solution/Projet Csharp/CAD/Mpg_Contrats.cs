﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Projet_Csharp.CAD
{
    /// <summary>
    /// Classe de mappage regroupant les requetes concernant les contrats
    /// </summary>
    class Mpg_Contrats
    {
        /// <summary>
        /// Obtient toutes les infos de tous les contrats
        /// </summary>
        /// <returns></returns>
        public string m_GetContrats()
        {
            return "SELECT * FROM Contrat";
        }
    }
}
