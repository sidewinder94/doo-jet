﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Projet_Csharp.CAD
{
    /// <summary>
    /// Mappage client qui permet de gérer toute les demandes sur le client
    /// </summary>
    class Mpg_Client
    {
        /// <summary>
        /// Affiche tout les clients
        /// </summary>
        /// <returns></returns>
        public string m_GetAllCustomersName()
        {
            return "SELECT Nom, Prénom FROM Client";
        }
        /// <summary>
        /// Affiche le détail d'un client suivant son nom
        /// </summary>
        /// <param name="nom"></param>
        /// <returns></returns>
        public string m_GetCustomersByName(string nom)
        {
            return "SELECT Client.Nom, Client.Prénom, Client.Adresse, Client.[N° Telephone], Client.[Date naissance], Client.[N° Permis Cotier] FROM Client WHERE [Nom] = '" + nom + "';";
        }
        /// <summary>
        /// Sélection l'id d'un client par son nom et prénom
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <returns></returns>
        public string m_GetCustomerID(string firstName, string lastName)
        {
            return "SELECT Client.[N° Client] FROM Client WHERE (((Client.Nom)=\""+lastName+"\") AND ((Client.Prénom)=\""+firstName+"\"));";
        }
        /// <summary>
        /// Insert dans la base de données un nouveau client avec les données le concernant
        /// </summary>
        /// <param name="prenom"></param>
        /// <param name="nom"></param>
        /// <param name="adresse"></param>
        /// <param name="tel"></param>
        /// <param name="datenaissance"></param>
        /// <param name="permiscotier"></param>
        /// <returns></returns>
        public string m_AjouterClient(string prenom, string nom, string adresse, string tel, string datenaissance, string permiscotier)
        {
            return "INSERT INTO Client ([Prénom],[Nom],[Adresse],[N° Telephone],[Date naissance],[N° Permis Cotier])" + " VALUES ('" + prenom + "','" + nom + "','" + adresse + "','" + tel + "','" + datenaissance + "','" + permiscotier + "');";
        }
        /// <summary>
        /// Modifie la base de données d'après le nom d'un client et qui concerne ce client
        /// </summary>
        /// <param name="prenom"></param>
        /// <param name="nom"></param>
        /// <param name="adresse"></param>
        /// <param name="tel"></param>
        /// <param name="datenaissance"></param>
        /// <param name="permiscotier"></param>
        /// <returns></returns>
        public string m_MofifierClient(string prenom, string nom, string adresse, string tel, string datenaissance, string permiscotier)
        {
            return "UPDATE Client SET Client.Prénom = '" + prenom + "', Client.Nom = '" + nom + "', Client.Adresse = '" + adresse + "', Client.[N° Telephone] = '" + tel + "', Client.[Date naissance] = '" + datenaissance + "', Client.[N° Permis Cotier] = '" + permiscotier + "' WHERE Nom = '"+nom+"';";
        }
        /// <summary>
        /// Supprime de la base de données un client
        /// </summary>
        /// <param name="nom"></param>
        /// <returns></returns>
        public string m_SupprimerClient(string nom)
        {
            return "DELETE * FROM Client WHERE Nom = '"+nom+"';";
        }
        /// <summary>
        /// Sélectionne le nom du client suivant son id
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public string m_GetCustomerLNByID(int customerID)
        {
            return "SELECT Client.Nom FROM Client WHERE [N° Client] = " + customerID + ";";
        }
        /// <summary>
        /// Sélectionne le prénom du client suivant son id
        /// </summary>
        /// <param name="customerID"></param>
        /// <returns></returns>
        public string m_GetCustomerFNByID(int customerID)
        {
            return "SELECT Client.Prénom FROM Client WHERE [N° Client] = " + customerID + ";";
        }
        /// <summary>
        /// Permet d'obtenir toutes les infos d'un client d'apres son nom et prénom
        /// </summary>
        /// <param name="nom"></param>
        /// <param name="prenom"></param>
        /// <returns></returns>
        public string m_GetCustomer(string nom, string prenom)
        {
            return "SELECT * FROM Client WHERE (((Client.Nom)=\"" + nom + "\") AND ((Client.Prénom)=\"" + prenom + "\"));";
        }
    }
}
