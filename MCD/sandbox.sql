CREATE TABLE `Participer` (
  `N° Reservation`,
  `N° Effectif`,
  PRIMARY KEY(`N° Reservation`, `N° Effectif`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `Effectif` (
  `N° Effectif`,
  `N° Securité sociale`,
  `Date d embauche`,
  `Date Dernière visite médicale`,
  `N° Permis Côtier`,
  `N° BEES`,
  `Statut activité`,
  `N° Contrat`,
  PRIMARY KEY(`N° Effectif`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `Equiper` (
  `N° Reservation`,
  `N° Identifcation`,
  `Coût`,
  `nombre`,
  PRIMARY KEY(`N° Reservation`, `N° Identifcation`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `Activité` (
  `N° Type`,
  `Nom Type`,
  PRIMARY KEY(`N° Type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `Equipement` (
  `N° Identifcation`,
  `Numéro série`,
  `Nom`,
  `Descriptif`,
  `Puissance`,
  `disponibilité`,
  `quantité`,
  `cout HT location`,
  `N° Type`,
  PRIMARY KEY(`N° Identifcation`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `Client` (
  `N° Client`,
  `Nom`,
  `Prenom`,
  `Adresse`,
  `N° Telephone`,
  `Date Naissance`,
  `N° Permis cotier`,
  `Nombre reservations`,
  `Coût Reservations`,
  PRIMARY KEY(`N° Client`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `Contrat` (
  `N° Contrat`,
  `Type Contrat`,
  PRIMARY KEY(`N° Contrat`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `Reservation` (
  `N° Reservation`,
  `Date début`,
  `Heure début`,
  `Date fin`,
  `Heure Fin`,
  `coût`,
  `N° Client`,
  PRIMARY KEY(`N° Reservation`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE `Abscences` (
  `N° Abscence`,
  `Date début`,
  `heure début`,
  `Date fin`,
  `heure fin`,
  `motif`,
  `N° Effectif`,
  PRIMARY KEY(`N° Abscence`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

ALTER TABLE `Participer` ADD FOREIGN KEY (`N° Reservation`) REFERENCES `Reservation` (`N° Reservation`);

ALTER TABLE `Participer` ADD FOREIGN KEY (`N° Effectif`) REFERENCES `Effectif` (`N° Effectif`);

ALTER TABLE `Effectif` ADD FOREIGN KEY (`N° Contrat`) REFERENCES `Contrat` (`N° Contrat`);

ALTER TABLE `Equiper` ADD FOREIGN KEY (`N° Reservation`) REFERENCES `Reservation` (`N° Reservation`);

ALTER TABLE `Equiper` ADD FOREIGN KEY (`N° Identifcation`) REFERENCES `Equipement` (`N° Identifcation`);

ALTER TABLE `Equipement` ADD FOREIGN KEY (`N° Type`) REFERENCES `Activité` (`N° Type`);

ALTER TABLE `Reservation` ADD FOREIGN KEY (`N° Client`) REFERENCES `Client` (`N° Client`);

ALTER TABLE `Abscences` ADD FOREIGN KEY (`N° Effectif`) REFERENCES `Effectif` (`N° Effectif`);